<?php

namespace Someline\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AffiliateBilling extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;
    public $files;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invoice,$files)
    {
        $this->invoice = $invoice;
        $this->files = $files;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $platform_url ='http://sa.appone.link';
        $platform = 'SELECTAD GROUP LIMITED';
        $name = 'Selectad';
        if($this->invoice['affiliate']['platform'] === 'adbink'){
            $platform_url = "http://adb.appone.link";
            $platform = 'Adbink Limited';
            $name = 'Adbink';
        }

        $subject = 'Billing numbers for '.Carbon::parse($this->invoice['month'])->format('M Y').' From '.$platform;

        $email = $this->markdown('emails.affiliate_billing')
            ->from('support@appone.link',$name)
            ->subject($subject)
            ->attach($this->invoice["link"]);

        foreach($this->files as $file){
            $email->attach($file);
        }
        return $email->with([
                'invoice'=> $this->invoice,
                'platform_url'=> $platform_url
            ]);
    }
}
