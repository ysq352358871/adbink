<?php

namespace Someline\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdvertiserInvoice extends Mailable
{
    use Queueable, SerializesModels;

    public $adv;
    public $invoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($adv,$invoice)
    {
        $this->adv = $adv;
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $platform_url ='http://sa.appone.link';
        $name = 'Selectad';

        if($this->adv->platform === 'adbink'){
            $platform_url = "http://adb.appone.link";
            $name = 'Adbink';
        }

        $invoice_no = 'INV-NO-'.Carbon::parse($this->invoice['month'])->format('Ym').'-'.$this->adv->id;

        return $this->markdown('emails.advertiser_invoice_email')
            ->from('support@appone.link',$name)
            ->subject('Invoice Notification')
            ->attach($this->invoice["link"])
            ->with([
                'adv'=> $this->adv,
                'invoice'=> $this->invoice,
                'platform_url'=> $platform_url,
                'invoice_no'=> $invoice_no
            ]);
    }
}
