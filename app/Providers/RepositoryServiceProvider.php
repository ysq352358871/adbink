<?php

namespace Someline\Providers;

use Illuminate\Support\ServiceProvider;
use Someline\Repositories\Eloquent\UserRepositoryEloquent;
use Someline\Repositories\Interfaces\UserRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\CampaignRepository::class, \Someline\Repositories\Eloquent\CampaignRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\CountryRepository::class, \Someline\Repositories\Eloquent\CountryRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\AdvertiserRepository::class, \Someline\Repositories\Eloquent\AdvertiserRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\AffiliateRepository::class, \Someline\Repositories\Eloquent\AffiliateRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\RelationRepository::class, \Someline\Repositories\Eloquent\RelationRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\JumpRepository::class, \Someline\Repositories\Eloquent\JumpRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\EventRepository::class, \Someline\Repositories\Eloquent\EventRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\SummarryRepository::class, \Someline\Repositories\Eloquent\SummarryRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\BlacklistRepository::class, \Someline\Repositories\Eloquent\BlacklistRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\ApplyRepository::class, \Someline\Repositories\Eloquent\ApplyRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\PostbackRepository::class, \Someline\Repositories\Eloquent\PostbackRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\BillingRepository::class, \Someline\Repositories\Eloquent\BillingRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\InvoiceRepository::class, \Someline\Repositories\Eloquent\InvoiceRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\AutoRepository::class, \Someline\Repositories\Eloquent\AutoRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\BusinessRepository::class, \Someline\Repositories\Eloquent\BusinessRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\OpmarkRepository::class, \Someline\Repositories\Eloquent\OpmarkRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\ScheduleRepository::class, \Someline\Repositories\Eloquent\ScheduleRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\DeductRepository::class, \Someline\Repositories\Eloquent\DeductRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\OutlayRepository::class, \Someline\Repositories\Eloquent\OutlayRepositoryEloquent::class);
        //:end-bindings:
    }
}
