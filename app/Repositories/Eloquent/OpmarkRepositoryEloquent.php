<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\OpmarkRepository;
use Someline\Models\Opmark;
use Someline\Validators\OpmarkValidator;
use Someline\Presenters\OpmarkPresenter;

/**
 * Class OpmarkRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class OpmarkRepositoryEloquent extends BaseRepository implements OpmarkRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Opmark::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return OpmarkValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return OpmarkPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
