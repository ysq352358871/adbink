<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\JumpRepository;
use Someline\Models\Jump;
use Someline\Validators\JumpValidator;
use Someline\Presenters\JumpPresenter;

/**
 * Class JumpRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class JumpRepositoryEloquent extends BaseRepository implements JumpRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Jump::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return JumpValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return JumpPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
