<?php

namespace Someline\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\OutlayRepository;
use Someline\Models\Outlay;
use Someline\Validators\OutlayValidator;

/**
 * Class OutlayRepositoryEloquent.
 *
 * @package namespace Someline\Repositories\Eloquent;
 */
class OutlayRepositoryEloquent extends BaseRepository implements OutlayRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Outlay::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
