<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\BlacklistRepository;
use Someline\Models\Blacklist;
use Someline\Validators\BlacklistValidator;
use Someline\Presenters\BlacklistPresenter;

/**
 * Class BlacklistRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class BlacklistRepositoryEloquent extends BaseRepository implements BlacklistRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Blacklist::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BlacklistValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return BlacklistPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
