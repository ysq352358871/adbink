<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\AutoRepository;
use Someline\Models\Auto;
use Someline\Validators\AutoValidator;
use Someline\Presenters\AutoPresenter;

/**
 * Class AutoRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class AutoRepositoryEloquent extends BaseRepository implements AutoRepository
{

    protected $fieldSearchable = [
        'billing.id',
        'billing.name'=>'like',
        'affiliate.id',
        'affiliate.name' => 'like',
        'package_name'=> 'like',
        'jump'=> '<=',
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Auto::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AutoValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return AutoPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
