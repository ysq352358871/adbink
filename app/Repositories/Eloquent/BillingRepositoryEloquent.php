<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\BillingRepository;
use Someline\Models\Billing;
use Someline\Validators\BillingValidator;
use Someline\Presenters\BillingPresenter;

/**
 * Class BillingRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class BillingRepositoryEloquent extends BaseRepository implements BillingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Billing::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BillingValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return BillingPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
