<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\PostbackRepository;
use Someline\Models\Postback;
use Someline\Validators\PostbackValidator;
use Someline\Presenters\PostbackPresenter;

/**
 * Class PostbackRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class PostbackRepositoryEloquent extends BaseRepository implements PostbackRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Postback::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PostbackValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return PostbackPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
