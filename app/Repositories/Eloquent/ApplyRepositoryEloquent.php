<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\ApplyRepository;
use Someline\Models\Apply;
use Someline\Validators\ApplyValidator;
use Someline\Presenters\ApplyPresenter;

/**
 * Class ApplyRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class ApplyRepositoryEloquent extends BaseRepository implements ApplyRepository
{
    protected $fieldSearchable = [
        'campaign.id',
        'campaign.name'=>'like',
        'affiliate.id',
        'affiliate.name' => 'like',
        'campaign.package_name'=> 'like',
    ];




    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Apply::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ApplyValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return ApplyPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
