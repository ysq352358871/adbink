<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\SummarryRepository;
use Someline\Models\Summarry;
use Someline\Validators\SummarryValidator;
use Someline\Presenters\SummarryPresenter;

/**
 * Class SummarryRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class SummarryRepositoryEloquent extends BaseRepository implements SummarryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Summarry::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return SummarryValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return SummarryPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
