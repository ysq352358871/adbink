<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\AdvertiserRepository;
use Someline\Models\Advertiser;
use Someline\Validators\AdvertiserValidator;
use Someline\Presenters\AdvertiserPresenter;

/**
 * Class AdvertiserRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class AdvertiserRepositoryEloquent extends BaseRepository implements AdvertiserRepository
{
    protected $fieldSearchable = [
        'name' => 'like',
        'id',
        'email' => 'like',
        'company_name' => 'like',
        'contact_name' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Advertiser::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AdvertiserValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return AdvertiserPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
