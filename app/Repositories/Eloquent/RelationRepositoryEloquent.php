<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\RelationRepository;
use Someline\Models\Relation;
use Someline\Validators\RelationValidator;
use Someline\Presenters\RelationPresenter;

/**
 * Class RelationRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class RelationRepositoryEloquent extends BaseRepository implements RelationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Relation::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RelationValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return RelationPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
