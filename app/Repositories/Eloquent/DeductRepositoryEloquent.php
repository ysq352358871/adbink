<?php

namespace Someline\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\DeductRepository;
use Someline\Models\Deduct;
use Someline\Validators\DeductValidator;

/**
 * Class DeductRepositoryEloquent.
 *
 * @package namespace Someline\Repositories\Eloquent;
 */
class DeductRepositoryEloquent extends BaseRepository implements DeductRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Deduct::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
