<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface AutoRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface AutoRepository extends BaseRepositoryInterface
{
    //
}
