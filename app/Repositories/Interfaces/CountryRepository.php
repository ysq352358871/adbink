<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface CountryRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface CountryRepository extends BaseRepositoryInterface
{
    //
}
