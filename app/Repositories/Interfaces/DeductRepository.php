<?php

namespace Someline\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DeductRepository.
 *
 * @package namespace Someline\Repositories\Interfaces;
 */
interface DeductRepository extends RepositoryInterface
{
    //
}
