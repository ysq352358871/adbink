<?php

namespace Someline\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OutlayRepository.
 *
 * @package namespace Someline\Repositories\Interfaces;
 */
interface OutlayRepository extends RepositoryInterface
{
    //
}
