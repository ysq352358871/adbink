<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface ScheduleRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface ScheduleRepository extends BaseRepositoryInterface
{
    //
}
