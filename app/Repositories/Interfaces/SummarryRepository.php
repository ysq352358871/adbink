<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface SummarryRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface SummarryRepository extends BaseRepositoryInterface
{
    //
}
