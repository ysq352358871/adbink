<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface BusinessRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface BusinessRepository extends BaseRepositoryInterface
{
    //
}
