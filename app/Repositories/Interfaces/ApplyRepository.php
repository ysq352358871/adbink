<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface ApplyRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface ApplyRepository extends BaseRepositoryInterface
{
    //
}
