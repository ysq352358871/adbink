<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface RelationRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface RelationRepository extends BaseRepositoryInterface
{
    //
}
