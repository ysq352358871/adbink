<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface CampaignRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface CampaignRepository extends BaseRepositoryInterface
{
    //
}
