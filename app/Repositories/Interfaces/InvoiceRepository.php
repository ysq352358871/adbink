<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface InvoiceRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface InvoiceRepository extends BaseRepositoryInterface
{
    //
}
