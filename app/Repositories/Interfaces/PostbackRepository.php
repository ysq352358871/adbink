<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface PostbackRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface PostbackRepository extends BaseRepositoryInterface
{
    //
}
