<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface EventRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface EventRepository extends BaseRepositoryInterface
{
    //
}
