<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface AdvertiserRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface AdvertiserRepository extends BaseRepositoryInterface
{
    //
}
