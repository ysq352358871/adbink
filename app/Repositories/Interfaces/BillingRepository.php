<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface BillingRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface BillingRepository extends BaseRepositoryInterface
{
    //
}
