<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface OpmarkRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface OpmarkRepository extends BaseRepositoryInterface
{
    //
}
