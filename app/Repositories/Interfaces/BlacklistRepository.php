<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface BlacklistRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface BlacklistRepository extends BaseRepositoryInterface
{
    //
}
