<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface JumpRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface JumpRepository extends BaseRepositoryInterface
{
    //
}
