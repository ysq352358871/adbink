<?php

namespace Someline\Transformers;

use Someline\Models\Advertiser;

/**
 * Class AdvertiserTransformer
 * @package namespace Someline\Transformers;
 */
class AdvertiserTransformer extends BaseTransformer
{

    /**
     * Transform the Advertiser entity
     * @param Advertiser $model
     *
     * @return array
     */
    public function transform(Advertiser $model)
    {
        return [
            'id' => (int) $model->id,
            'user_id' => (int) $model->user_id,
            'name' => $model->name,
            'platform' => $model->platform,
            'email' => $model->email,
            'margin' => $model->margin,
            'discount' => $model->discount,
            'mark' => $model->mark,
            'live' => $model->live,
            'company_name' => $model->company_name,
            'contact_name' => $model->contact_name,
            'im' => $model->im,
            'address' => $model->address,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
