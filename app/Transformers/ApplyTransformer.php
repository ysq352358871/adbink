<?php

namespace Someline\Transformers;

use Someline\Base\Models\BaseModel;
use Someline\Models\Apply;

/**
 * Class ApplyTransformer
 * @package namespace Someline\Transformers;
 */
class ApplyTransformer extends BaseTransformer
{

    protected $availableIncludes = [
        'campaign','affiliate'
    ];

    /**
     * Transform the Apply entity
     * @param Apply $model
     *
     * @return array
     */
    public function transform(Apply $model)
    {
        return [
            'id' => (int) $model->id,
            'campaign_id' => (int) $model->campaign_id,
            'customer_id' => (int) $model->customer_id,
            'status' => (int) $model->status,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }


    public function includeCampaign(BaseModel $model)
    {
        $campaign = $model->campaign; //advertiser
        if ($campaign) {
            return $this->item($campaign, new CampaignTransformer());
        }
        return null;
    }

    public function includeAffiliate(BaseModel $model)
    {
        $affiliate = $model->affiliate; //advertiser
        if ($affiliate) {
            return $this->item($affiliate, new AffiliateTransformer());
        }
        return null;
    }
}
