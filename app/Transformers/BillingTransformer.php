<?php

namespace Someline\Transformers;

use Someline\Models\Billing;

/**
 * Class BillingTransformer
 * @package namespace Someline\Transformers;
 */
class BillingTransformer extends BaseTransformer
{

    /**
     * Transform the Billing entity
     * @param Billing $model
     *
     * @return array
     */
    public function transform(Billing $model)
    {
        return [
            'id' => (int) $model->id,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
