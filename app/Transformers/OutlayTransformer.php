<?php

namespace Someline\Transformers;

use League\Fractal\TransformerAbstract;
use Someline\Models\Outlay;

/**
 * Class OutlayTransformer.
 *
 * @package namespace Someline\Transformers;
 */
class OutlayTransformer extends TransformerAbstract
{
    /**
     * Transform the Outlay entity.
     *
     * @param \Someline\Models\Outlay $model
     *
     * @return array
     */
    public function transform(Outlay $model)
    {
        return [
            'id'         => (int) $model->id,
            'month'=> $model->month,
            'type'=> $model->type,
            'amount' => $model->amount,
            'currency' => $model->currency,
            'mark' => $model->mark,
            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
