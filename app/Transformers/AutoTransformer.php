<?php

namespace Someline\Transformers;

use Someline\Base\Models\BaseModel;
use Someline\Models\Auto;

/**
 * Class AutoTransformer
 * @package namespace Someline\Transformers;
 */
class AutoTransformer extends BaseTransformer
{
    protected $availableIncludes = [
        'billing','affiliate'
    ];

    /**
     * Transform the Auto entity
     * @param Auto $model
     *
     * @return array
     */
    public function transform(Auto $model)
    {
        return [
            'id' => (int) $model->id,
            'advertiser' => (int) $model->advertiser,
            'customer' => (int) $model->customer,
            'package_name' => $model->package_name,
            'jump' => $model->jump,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }

    public function includeBilling(BaseModel $model)
    {
        $advertiser = $model->billing; //advertiser
        if ($advertiser) {
            return $this->item($advertiser, new AdvertiserTransformer());
        }
        return null;
    }


    public function includeAffiliate(BaseModel $model)
    {
        $affiliate = $model->affiliate; //advertiser
        if ($affiliate) {
            return $this->item($affiliate, new AffiliateTransformer());
        }
        return null;
    }
}
