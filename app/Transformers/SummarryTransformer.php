<?php

namespace Someline\Transformers;

use Someline\Base\Models\BaseModel;
use Someline\Models\Summarry;

/**
 * Class SummarryTransformer
 * @package namespace Someline\Transformers;
 */
class SummarryTransformer extends BaseTransformer
{
    protected $availableIncludes = [
        'campaign'
    ];
    /**
     * Transform the Summarry entity
     * @param Summarry $model
     *
     * @return array
     */
    public function transform(Summarry $model)
    {
        return [
            'id' => (int) $model->id,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }


    public function includeCampaign(BaseModel $model)
    {
        $campaign = $model->campaign;
        if ($campaign) {
            return $this->item($campaign, new CampaignTransformer());
        }
        return null;
    }
}
