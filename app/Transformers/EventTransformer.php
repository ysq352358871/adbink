<?php

namespace Someline\Transformers;

use Someline\Models\Event;

/**
 * Class EventTransformer
 * @package namespace Someline\Transformers;
 */
class EventTransformer extends BaseTransformer
{

    /**
     * Transform the Event entity
     * @param Event $model
     *
     * @return array
     */
    public function transform(Event $model)
    {
        return [
            'id' => (int) $model->id,
            'campaign_id' => (int) $model->campaign_id,
            'event_token' => $model->event_token,
            'active' => $model->active,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
