<?php

namespace Someline\Transformers;

use Someline\Models\Jump;

/**
 * Class JumpTransformer
 * @package namespace Someline\Transformers;
 */
class JumpTransformer extends BaseTransformer
{

    /**
     * Transform the Jump entity
     * @param Jump $model
     *
     * @return array
     */
    public function transform(Jump $model)
    {
        return [
            'id' => (int) $model->id,
            'jump_data'=> (int) $model->jump_data,
            'take'=> (int) $model->take,
            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
