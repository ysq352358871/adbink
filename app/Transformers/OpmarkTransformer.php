<?php

namespace Someline\Transformers;

use Someline\Base\Models\BaseModel;
use Someline\Models\Opmark;

/**
 * Class OpmarkTransformer
 * @package namespace Someline\Transformers;
 */
class OpmarkTransformer extends BaseTransformer
{

    protected $availableIncludes = [
        'user'
    ];

    /**
     * Transform the Opmark entity
     * @param Opmark $model
     *
     * @return array
     */
    public function transform(Opmark $model)
    {
        return [
            'id' => (int) $model->id,
            'user_id' => (int) $model->user_id,
            'type' => (string) $model->type,
            'name' => $model->name,
            'mark' => $model->mark,
            'status' => (int) $model->status,
            'top' => (int) $model->top,
            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }

    public function includeUser(BaseModel $model)
    {
        $user = $model->user; //advertiser
        if ($user) {
            return $this->item($user, new UserTransformer());
        }
        return null;
    }
}
