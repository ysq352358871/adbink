<?php

namespace Someline\Transformers;

use Someline\Models\Blacklist;

/**
 * Class BlacklistTransformer
 * @package namespace Someline\Transformers;
 */
class BlacklistTransformer extends BaseTransformer
{

    /**
     * Transform the Blacklist entity
     * @param Blacklist $model
     *
     * @return array
     */
    public function transform(Blacklist $model)
    {
        return [
            'id' => (int) $model->id,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
