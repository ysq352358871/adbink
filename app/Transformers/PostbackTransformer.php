<?php

namespace Someline\Transformers;

use Someline\Models\Postback;

/**
 * Class PostbackTransformer
 * @package namespace Someline\Transformers;
 */
class PostbackTransformer extends BaseTransformer
{

    /**
     * Transform the Postback entity
     * @param Postback $model
     *
     * @return array
     */
    public function transform(Postback $model)
    {
        return [
            'id' => (int) $model->id,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
