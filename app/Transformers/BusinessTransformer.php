<?php

namespace Someline\Transformers;

use Someline\Base\Models\BaseModel;
use Someline\Models\Business;

/**
 * Class BusinessTransformer
 * @package namespace Someline\Transformers;
 */
class BusinessTransformer extends BaseTransformer
{
    protected $availableIncludes = [
        'user'
    ];

    /**
     * Transform the Business entity
     * @param Business $model
     *
     * @return array
     */
    public function transform(Business $model)
    {
        $data = [
            'id' => (int) $model->id,
            'user_id' => (int) $model->user_id,
            'top' => (int) $model->top,
            'client_type' => $model->client_type,
            'name' => $model->name,
            'mark' => $model->mark,
//            'contact' => $model->contact,
            'status' => (int) $model->status,
            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
        if (\Auth::user() && (\Auth::user()->id === $model->id || \Auth::user()->role === 'admin')) {
            return array_merge($data, [
                'contact' => $model->contact,
            ]);
        }
        return $data;
    }

    public function includeUser(BaseModel $model)
    {
        $user = $model->user; //advertiser
        if ($user) {
            return $this->item($user, new UserTransformer());
        }
        return null;
    }
}
