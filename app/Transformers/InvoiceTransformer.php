<?php

namespace Someline\Transformers;

use Someline\Models\Invoice;

/**
 * Class InvoiceTransformer
 * @package namespace Someline\Transformers;
 */
class InvoiceTransformer extends BaseTransformer
{

    /**
     * Transform the Invoice entity
     * @param Invoice $model
     *
     * @return array
     */
    public function transform(Invoice $model)
    {
        return [
            'id' => (int) $model->id,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
