<?php

namespace Someline\Transformers;

use Someline\Models\Schedule;

/**
 * Class ScheduleTransformer
 * @package namespace Someline\Transformers;
 */
class ScheduleTransformer extends BaseTransformer
{

    /**
     * Transform the Schedule entity
     * @param Schedule $model
     *
     * @return array
     */
    public function transform(Schedule $model)
    {
        return [
            'id' => (int) $model->id,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
