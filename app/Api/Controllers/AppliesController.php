<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\ApplyCreateRequest;
use Someline\Http\Requests\ApplyUpdateRequest;
use Someline\Repositories\Interfaces\ApplyRepository;
use Someline\Validators\ApplyValidator;
use Someline\Models\Apply;
use Someline\Models\Relation;

class AppliesController extends BaseController
{

    /**
     * @var ApplyRepository
     */
    protected $repository;

    /**
     * @var ApplyValidator
     */
    protected $validator;

    public function __construct(ApplyRepository $repository, ApplyValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);

        return $this->repository->paginate($per_page);
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ApplyCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ApplyCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $apply = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $apply;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ApplyUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(ApplyUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $apply = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }

    // 运营批准申请
    public function applyOffer(ApplyUpdateRequest $request)
    {
        $now = Carbon::now('utc')->toDateTimeString();
        $data = $request->get('data');
        $approveData = $request->get('approve_data');
        $data['updated_at'] = $now;

        $is_batch = $request->get('is_batch',0);

        if(!!$is_batch){

            $ids = $request->get('ids');

            $user_id = $request->get('user_id');

            $data['updated_by'] = $user_id;

            Apply::whereIn('id', $ids)->update($data);

            if($data['status'] === 1){
                $this->storeApply($approveData);
            }

            return $this->response->noContent();
        }

        $id = $request->get('id');

        $apply = Apply::find($id);

        $apply->update($data);

        if($data['status'] === 1){
            $this->storeApply($approveData);
        }

        return $apply;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function apply(ApplyUpdateRequest $request)
    {
        $data = $request->get('data');

        $now = Carbon::now('utc')->toDateTimeString();
        foreach ($data as $key => $item) {
            $item['created_at'] = $now;
            $data[$key] = $item;
        }

        Apply::insert($data);

        return $this->response->noContent();
    }

    public function storeApply($data)
    {
        $now = Carbon::now('utc')->toDateTimeString();
        foreach ($data as $key => $item) {
            $item['created_at'] = $now;
            $data[$key] = $item;
        }
        Relation::insert($data);
    }
}
