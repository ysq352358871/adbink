<?php

namespace Someline\Api\Controllers;

use Illuminate\Http\Request;

use Someline\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Someline\Http\Requests\OutlayCreateRequest;
use Someline\Http\Requests\OutlayUpdateRequest;
use Someline\Repositories\Interfaces\OutlayRepository;
use Someline\Validators\OutlayValidator;

/**
 * Class OutlaysController.
 *
 * @package namespace Someline\Api\Controllers;
 */
class OutlaysController extends BaseController
{
    /**
     * @var OutlayRepository
     */
    protected $repository;

    /**
     * @var OutlayValidator
     */
    protected $validator;

    /**
     * OutlaysController constructor.
     *
     * @param OutlayRepository $repository
     * @param OutlayValidator $validator
     */
    public function __construct(OutlayRepository $repository, OutlayValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);
        $month = $request->get('month','');

        return $this->repository->where('month',$month)->orderBy('id', 'desc')->paginate($per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OutlayCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(OutlayCreateRequest $request)
    {
        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $outlay = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $outlay;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $outlay = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $outlay,
            ]);
        }

        return view('outlays.show', compact('outlay'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $outlay = $this->repository->find($id);

        return view('outlays.edit', compact('outlay'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OutlayUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(OutlayUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $outlay = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Outlay updated.',
                'data'    => $outlay->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Outlay deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Outlay deleted.');
    }
}
