<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\AdvertiserCreateRequest;
use Someline\Http\Requests\AdvertiserUpdateRequest;
use Illuminate\Support\Facades\Mail;
use Someline\Mail\AdvertiserInvoice;
use Someline\Models\Advertiser;
use Someline\Models\Invoice;
use Someline\Models\Summarry;
use Someline\Repositories\Interfaces\AdvertiserRepository;
use Someline\Validators\AdvertiserValidator;

class AdvertisersController extends BaseController
{

    /**
     * @var AdvertiserRepository
     */
    protected $repository;

    /**
     * @var AdvertiserValidator
     */
    protected $validator;

    public function __construct(AdvertiserRepository $repository, AdvertiserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);

        return $this->repository->orderBy('id', 'desc')->paginate($per_page);
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    public function weekly()
    {
        $data = Advertiser::with([
            'summarries' => function($query){
                $query->groupby('campaign','customer')
                    ->select(\DB::raw('SUM(postback) as postback'));
            },
            'total' => function($query){
                $query->groupby('laravel_through_key')
                    ->select(\DB::raw('SUM(postback) as postback'));
            }
        ])->get();
        return $data;
    }

    public function top(Request $request){
        $top = $request->get('top',50);
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];
        $user_id = $request->get('user_id');

        $data = Advertiser::query()->leftJoin('_tb_campaign AS c','_tb_advertiser.id','=','c.advertiser')
            ->leftJoin('_tb_summarry AS s','s.campaign','=','c.id')
            ->whereBetween(\DB::raw("date_format(s.date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
            ->when($user_id, function ($q) use ($user_id) {
                $q->where('_tb_advertiser.user_id',$user_id);
            })
            ->select(
                '_tb_advertiser.id as id',
                '_tb_advertiser.user_id as user_id',
                '_tb_advertiser.name as name',
                \DB::raw('SUM(s.tracking) as tracking'),
                \DB::raw('SUM(s.postback) as postback'),
                \DB::raw('SUM(s.callback) as callback'),
                \DB::raw('SUM(s.revenue) as revenue'),
                \DB::raw('SUM(s.payment) as payment'),
                \DB::raw('SUM(s.revenue) - SUM(s.payment) as profit')
            )
            ->groupBy('id')
            ->orderBy('revenue','desc')
            ->take($top)
            ->get();

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AdvertiserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertiserCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $advertiser = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $advertiser;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AdvertiserUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(AdvertiserUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $advertiser = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function sendInvoiceEmail(AdvertiserUpdateRequest $request,$id)
    {
        $invoice = $request->get('invoice');
        $advertiser = Advertiser::find($id);

        Mail::to($advertiser->email)
            ->cc('support@appone.link')
            ->send(new AdvertiserInvoice($advertiser,$invoice));

        $email_send_time = Carbon::now()->toDateTimeString();
        $update_invoice = Invoice::find($invoice["id"]*1);
        $update_invoice->email_send_time = $email_send_time;
        $update_invoice->save();

        return [
            "code"=>0,
            "data"=>$email_send_time
        ];
    }
}
