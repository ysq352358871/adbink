<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\CampaignCreateRequest;
use Someline\Http\Requests\CampaignUpdateRequest;
use Someline\Models\Relation;
use Someline\Repositories\Interfaces\CampaignRepository;
use Someline\Validators\CampaignValidator;
use Someline\Models\Campaign;
use Someline\Models\Affiliate;
use Someline\Models\Event;

class CampaignsController extends BaseController
{

    /**
     * @var CampaignRepository
     */
    protected $repository;

    /**
     * @var CampaignValidator
     */
    protected $validator;

    public function __construct(CampaignRepository $repository, CampaignValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);

        $advertiser =  $request->get('advertiser');
        $geo =  $request->get('geo');
        $jumps =  $request->get('jumps');
        $package_name =  $request->get('package_name');
        $take =  $request->get('take','');
        $adv_oid =  $request->get('adv_oid','');
        $live =  $request->get('live','');
        $is_api = $request->get('is_api',0);
        $pull = $request->get('pull','');
        $lead = $request->get('lead','');
        $today = $request->get('today','');
        $is_top = $request->get('is_top',0);
        $cost = $request->get('cost','');
        $ecpc = $request->get('ecpc','');
        $ecvr = $request->get('ecvr','');

        $re = $this->repository
            ->scopeQuery(function ($query) use ($advertiser,$geo,$jumps,$package_name,$take,$live,$is_api,$adv_oid,$pull,$lead,$today,$is_top,$cost,$ecpc,$ecvr) {
                $q = $query->when($geo, function ($sub_query) use ($geo) {
                    return $sub_query->where('geo', 'like', '%' . $geo . '%');
                })->when($jumps,function ($sub_query) use ($jumps){
                    $sub_query->whereHas('jump', function ($sub_sub_query) use ($jumps) {
                        return $sub_sub_query->where('jump_data', '<=', $jumps);
                    });
                })->when($package_name,function ($sub_query) use($package_name){
                    return $sub_query->where('package_name', 'like','%'.$package_name.'%');
                })->when($take,function ($sub_query) use ($take){
                    $sub_query->whereHas('jump', function ($sub_sub_query) use ($take) {
                        return $sub_sub_query->where('take', '=', $take - 1);
                    });
                })->when($live,function ($sub_query) use ($live){
                    return $sub_query->where('live', '=', $live - 1);
                })->when($adv_oid,function ($sub_query) use ($adv_oid){
                    return $sub_query->where('adv_oid',$adv_oid);
                })->when($pull,function ($sub_query) use ($pull){
                    return $sub_query->where('pull',$pull);
                })->when($lead,function ($sub_query) use ($lead){
                    return $sub_query->where('lead',$lead);
                })->when($today,function ($sub_query) use ($today){
                    return $sub_query->where('today',$today);
                });

                if(!!$is_top){
                    $sub_q = $q->when($ecpc,function ($sub_query) use ($ecpc){
                        return $sub_query->where([
                            ['ecpc','>=',$ecpc]
                        ]);
                    })->when($ecvr,function ($sub_query) use ($ecvr){
                        return $sub_query->where([
                            ['ecvr','>=',$ecvr]
                        ]);
                    })->when($advertiser,function ($sub_query) use($advertiser){
                        return $sub_query->whereIn('advertiser', $advertiser);
                    });

                    if(!!$cost){
                        $sub_q->orderBy('cost', 'desc')->take($cost);
                    }
                    return $sub_q;
                }else{
                    return $q->when($advertiser,function ($sub_query) use($advertiser){
                        return $sub_query->where('advertiser', $advertiser);
                    })->where('api',$is_api);
                }
            });
            if(!!$is_top){
                if(!!$cost){
                    $re->take($cost);
                    $all = $re->all();
                    $result = collect($all['data']);
                    // 手动分页
                    $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

                    $currentPageResult = $result->slice($currentPage * $per_page, $per_page)->values();
                    $paginatedResult = new LengthAwarePaginator($currentPageResult, $result->count(), $per_page);
                    $paginatedResult->setPath(\request()->url());
                    return response()->json([
                        'data' => $paginatedResult,
                    ]);
                }

                return $re->orderBy('cost', 'desc')->paginate($per_page);

            }
            return $re->orderBy('id', 'desc')->paginate($per_page);
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    public function search(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);

        return $this->repository->orderBy('id', 'desc')->paginate($per_page);
    }


    public function getEvents($id)
    {
        $events = Event::where('campaign_id',$id)->orderBy('id', 'desc')->get();
        return [
            'data'=>$events
        ];
    }


    public function getRelationAffiliates($id)
    {
//        $campaign = Campaign::find($id);

        $affiliates = Affiliate::query()
            ->join('_tb_relation as r',function ($q){
                return $q->on('r.customer','=','_tb_customer.id');
            })
            ->join('_tb_campaign as c',function ($q){
                return $q->on('r.campaign','=','c.id');
            })
            ->where('c.id',$id)
            ->select([
                '_tb_customer.id as id',
                '_tb_customer.name as name',
                '_tb_customer.margin as aff_margin',
                'r.margin as actual_margin',
                'r.price as price',
                'r.live as live',
                'r.status as status',
                'r.campaign as campaign',
                'r.customer as customer',
                'r.daily as daily',
                'r.pubx as pubx'
            ])->get();

//        $affiliates = $campaign->affiliates()->orderBy('id', 'desc')->get();

        return response()->json([
            'data' => $affiliates,
        ]);
    }

    public function top(Request $request)
    {
        $top = $request->get('top',50);
        $id = $request->get('id',''); // adv的id. 如果有: 查询某个adv下的top渠道
        $user_id = $request->get('user_id');

        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $query = Campaign::query();

        if($user_id || $id){
            $query = $query ->leftJoin('_tb_advertiser AS a','_tb_campaign.advertiser','=','a.id');
        }

        $data = $query->leftJoin('_tb_summarry AS s','_tb_campaign.id','=','s.campaign')
           ->whereBetween(\DB::raw("date_format(s.date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
           ->when($user_id,function ($q) use ($user_id){
               $q->where('a.user_id',$user_id);
           })
            ->when($id,function ($q) use ($id){
                $q->where('a.id',$id);
            })
           ->select(
                '_tb_campaign.id as id',
                '_tb_campaign.name as name',
                \DB::raw('SUM(s.tracking) as tracking'),
                \DB::raw('SUM(s.postback) as postback'),
                \DB::raw('SUM(s.callback) as callback'),
                \DB::raw('SUM(s.revenue) as revenue'),
                \DB::raw('SUM(s.payment) as payment'),
                \DB::raw('SUM(s.revenue) - SUM(payment) as profit')
           )
           ->groupBy('id')
           ->orderBy('revenue','desc')
           ->take($top)
           ->get();


        return response()->json([
            'data' => $data
        ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  CampaignCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $campaign = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $campaign;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        return Campaign::find($id)->billing;
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CampaignUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(CampaignUpdateRequest $request, $id)
    {

        $data = $request->all();
//        $offer = Campaign::find($id);
//
//        if(!$offer->live && !!$data['live']){
//            $now = Carbon::now()->toDateTimeString();
//            $data = array_merge($data,array("created_at"=>$now));
//        }

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $campaign = $this->repository->update($data, $id);

        if(!!$data['pubkey']){
            $array = array("pubx"=>0);
            Relation::where([
                ['campaign','=',$id],
                ['pubx','=',1]
            ])->update($array);
        }

        return $campaign;

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }

    public function updateStatus(CampaignUpdateRequest $request)
    {
        $ids = $request->get('ids');
        $data = $request->get('data');

        if(!!$data['live']){
            $data['created_at'] = Carbon::now()->toDateTimeString();
        }

        Campaign::whereIn('id', $ids)->update($data);

        return $this->response->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function getOffers(Request $request)
    {
        $id = $request->get('c_id');
        $search = $request->get('search');

        $perPage = $request->get('paginationPerPage',20);

        $geo =  $request->get('geo');

        $data = Campaign::query()
            ->leftJoin('_tb_apply',function($q) use ($id){
                $q->on('_tb_campaign.id','=','_tb_apply.campaign_id')
                    ->where('_tb_apply.customer_id',$id);
            })
            ->leftJoin('_tb_relation',function ($q) use ($id){
            $q->on('_tb_campaign.id','=','_tb_relation.campaign')
                ->where('_tb_relation.customer',$id);
        })
            ->select(
                'name','_tb_campaign.id as id','geo','package_name','os','_tb_campaign.price as price',
                '_tb_campaign.daily as daily','_tb_campaign.live as live',
                '_tb_campaign.status as status'
            )
            ->when($geo, function ($sub_query) use ($geo) {
                return $sub_query->where('geo', 'like', '%' . $geo . '%');
            })
            ->when($search, function ($sub_query) use ($search) {
                return $sub_query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('package_name', 'like', '%' . $search . '%');
            })
            ->where([
                ['_tb_relation.id','=',null],
                ['_tb_apply.id','=',null]
            ])
            ->where('_tb_campaign.live','=',1)
            ->orderBy('id', 'desc')->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    public function canGetTrackingLink(Request $request)
    {
        $aff_id = $request->get('aff_id');
        $offer_id = $request->get('offer_id');

        $results = Relation::where([
            ['campaign','=',$offer_id],
            ['customer','=',$aff_id]
        ])->get();
        return count($results);
    }
}
