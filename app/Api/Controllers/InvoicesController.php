<?php

namespace Someline\Api\Controllers;

use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Database\Query\Builder as QBuilder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\InvoiceCreateRequest;
use Someline\Http\Requests\InvoiceUpdateRequest;
use Someline\Models\Billing;
use Someline\Models\Outlay;
use Someline\Repositories\Interfaces\InvoiceRepository;
use Someline\Validators\InvoiceValidator;
use Someline\Models\Invoice;
use Carbon\Carbon;

class InvoicesController extends BaseController
{

    /**
     * @var InvoiceRepository
     */
    protected $repository;

    /**
     * @var InvoiceValidator
     */
    protected $validator;

    public function __construct(InvoiceRepository $repository, InvoiceValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$type)
    {
        $per_page = $request->get('paginationPerPage',20);

        $advertiser_id = $request->get('advertiser');

        $aff_id = $request->get('affiliate');

        $filter_month = $request->get('filter_month');

        $overdue = (int)$request->get('overdue','');
        $paid = (int)$request->get('paid','');

        $today = Carbon::now()->subDays(30);

        if($type === 'aff'){
            /* *
             *
             * 待对账(pending)的查询
             *
             * */
            $temp_a=Billing::where([
                ['status','=',0],
                ['date','=',$filter_month]
            ])
                ->groupBy('date','customer')
                ->selectRaw('date,customer,SUM(payment) as payment');

            /* *
            *
            * 已经对账(confirm)的自查询
            *
            * */
            $temp_b=Billing::where([
                ['status','=',1],
                ['date','=',$filter_month]
            ])
                ->groupBy('date','customer')
                ->selectRaw('date,customer,SUM(payment) - SUM(pub_reduce) as confirm,SUM(cover_amount) as cover_amount');

            $data = Invoice::with([
                'affiliate'=>function($q){
                    $q->select('id','name','email','platform','company_name');
                }
            ])
                ->join('_tb_billing as b',function ($q){
                    return $q->on('b.date','=','_tb_invoice.month')
                        ->on('b.customer','=','_tb_invoice.customer');
                })
                ->leftJoinSub($temp_a,'table_a',function(QBuilder $query){
                    $q = $query->on('_tb_invoice.month','=','table_a.date')
                        ->on('_tb_invoice.customer','=','table_a.customer');
                    return $q;
                })
                ->leftJoinSub($temp_b,'table_b',function(QBuilder $query){
                    $q = $query->on('_tb_invoice.month','=','table_b.date')
                        ->on('_tb_invoice.customer','=','table_b.customer');
                    return $q;
                })
                ->where('_tb_invoice.customer','>',0)
                ->when($aff_id,function ($q) use($aff_id){
                    $q->where('_tb_invoice.customer',$aff_id);
                })
                ->when($filter_month,function ($q) use($filter_month){
                    $q->where('month','=',$filter_month);
                })
                ->when($paid,function ($q) use ($paid){
                    $q->where('paid','=',$paid - 1);
                })
                ->when($overdue,function ($q) use ($overdue,$today){
                    if(!($overdue-1)){
                        $q->where('email_send_time',null)
                            ->orWhere([
                                ['email_send_time','!=',null],
                                ['email_send_time','>',$today]
                            ]);
                    }else{
                        $q->where([
                            ['email_send_time','!=',null],
                            ['email_send_time','<',$today]
                        ]);
                    }

                })
                ->groupBy('id','month','customer')
                ->orderby('id','desc')
                ->get([
                    '_tb_invoice.id as id','month','_tb_invoice.customer as customer','amount',
                    'pay_method','pay_amount','link','aff_invoice_link','email_send_time','paid',
                    \DB::raw('SUM(b.pub_reduce) as reduce'),
                    'table_a.payment as pending',
                    'table_b.confirm as confirm',
                    'table_b.cover_amount as cover_amount',
                    'mark',
                    \DB::raw("case when email_send_time > 0 and DATEDIFF(now(),email_send_time) > 30 then 1 else 0 end as overdue")
                ]);
        }else{



            $data = Invoice::with([
                'advertiser'=>function($q){
                    $q->select('id','name','company_name','address');
                }
            ])
                ->where('advertiser','>',0)
                ->when($advertiser_id,function ($q) use($advertiser_id){
                    $q->where('advertiser',$advertiser_id);
                })
                ->when($filter_month,function ($q) use($filter_month){
                    $q->where('month','=',$filter_month);
                })
                ->when($paid,function ($q) use ($paid){
                    $q->where('paid','=',$paid - 1);
                })
                ->when($overdue,function ($q) use ($overdue,$today){
                    if(!($overdue-1)){
                        $q->where('email_send_time',null)
                            ->orWhere([
                                ['email_send_time','!=',null],
                                ['email_send_time','>',$today]
                            ]);
                    }else{
                        $q->where([
                            ['email_send_time','!=',null],
                            ['email_send_time','<',$today]
                        ]);
                    }

                })
                ->orderby('id','desc')
                ->get([
                    'id','month','advertiser','amount','pay_method','pay_amount',
                    'link','email_send_time','mark','invoice_date','paid',
                    \DB::raw("case when email_send_time > 0 and DATEDIFF(now(),email_send_time) > 30 then 1 else 0 end as overdue"),
                ]);
        }


        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $per_page, $per_page)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $per_page);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  InvoiceCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $invoice = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $invoice;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  InvoiceUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(InvoiceUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $invoice = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    public function getOutlays(InvoiceUpdateRequest $request)
    {
//        $month = '2020-12';
        $month = $request->get('month','');
        $per_page = $request->get('paginationPerPage',12);


        $queryAdv = Invoice::query();
        if($month){
            $queryAdv = $queryAdv->where([
                ['month','=',$month],
                ['advertiser','>',0]
            ]);
        }else{
            $queryAdv = $queryAdv->where([
                ['advertiser','>',0]
            ]);
        }
        $temp_advertiser = $queryAdv->groupBy('month')->selectRaw('month as advertiser_month,SUM(amount) as final_revenue,SUM(pay_amount) as pay_revenue');

        $queryOther = Outlay::query();
        if($month){
            $queryOther = $queryOther->where('month',$month);
        }
        $temp_other = $queryOther->groupBy('month')
            ->selectRaw("month as other_month,CAST(SUM(case when currency = 'cny' then amount/6.5 else amount end) as DECIMAL(18,3)) as other_amount")
            ->getQuery();

        $query = Invoice::query();
        if($month){
            $query = $query->where([
                ['month','=',$month],
                ['customer','>',0]
            ]);
        }else{
            $query = $query->where([
                ['customer','>',0]
            ]);
        }
        $data = $query
            ->groupBy('month')
            ->joinSub($temp_advertiser, 'advertiser', 'advertiser.advertiser_month', '=', '_tb_invoice.month')
            ->joinSub($temp_other, 'other', 'other.other_month', '=', '_tb_invoice.month')
            ->get([
                '_tb_invoice.month as month',
                'final_revenue',
                'pay_revenue',
                \DB::raw('SUM(amount) as final_amount'),
                \DB::raw('SUM(pay_amount) as pay_amount'),
                'other_amount'
            ]);

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $per_page, $per_page)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $per_page);
        $paginatedResult->setPath(\request()->url());
        return $paginatedResult;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }
}
