<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\PostbackCreateRequest;
use Someline\Http\Requests\PostbackUpdateRequest;
use Someline\Models\Postback;
use Someline\Repositories\Interfaces\PostbackRepository;
use Someline\Validators\PostbackValidator;

class PostbacksController extends BaseController
{

    /**
     * @var PostbackRepository
     */
    protected $repository;

    /**
     * @var PostbackValidator
     */
    protected $validator;

    public function __construct(PostbackRepository $repository, PostbackValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->repository->paginate();
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }


    public function getConversionReport(PostbackCreateRequest $request)
    {

        $perPage = $request->get('paginationPerPage',20);
        $advertiser = $request->get('advertiser');
//        $offer_name = $request->get('offer_name');
        $offers = $request->get('offers');
        $package_name = $request->get('package_name');
        $affiliate = $request->get('affiliate');
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];
        $filed = $request->input('filed','date');
        $sort_type = $request->input('sort_type','desc');

//        $group = array( 'campaign','customer');

        $getFiled=array(
            'funny',
            'campaign',
            'customer',
            'aff_pub',
            'revenue',
            'payment',
            'margin',
            \DB::raw("date_format(date,'%Y-%m-%d') as date"),
            'take',
            'affclick',
            'idfa',
            'gaid'
        );

        $query = Postback::with([
            'campaign' => function($query){
                $query->select('id','name','advertiser','package_name');
            },
            'campaign.billing' => function ($query){
                $query->select('id','name');
            },
            'affiliate' => function($query){
                $query->select('id','name');
            }
        ])
            ->when($advertiser, function ($query) use ($advertiser) {
                return $query->advertiser($advertiser);
            })
            ->when($offers, function ($query) use ($offers) {
                $query->whereHas('campaign', function ($sub_query) use ($offers) {
                    $sub_query->whereIn('id',$offers);
                });
            })
            ->when($package_name, function ($query) use ($package_name) {
                $query->whereHas('campaign', function ($sub_query) use ($package_name) {
                    $sub_query->where('package_name', 'like', '%' . $package_name . '%');
                });
            })
            ->when($affiliate, function ($query) use ($affiliate) {
                $query->where('customer',$affiliate);
            })
            ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);

//        ->groupBy($group)
        $data = $query
            ->orderBy($filed,$sort_type)
            ->get($getFiled);

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  PostbackCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PostbackCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $postback = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $postback;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PostbackUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(PostbackUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $postback = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }
}
