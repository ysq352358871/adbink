<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\BillingCreateRequest;
use Someline\Http\Requests\BillingUpdateRequest;
use Someline\Models\Invoice;
use Someline\Repositories\Interfaces\BillingRepository;
use Someline\Validators\BillingValidator;
use Someline\Models\Billing;
use Someline\Models\Deduct;
use Someline\Models\Advertiser;

class BillingsController extends BaseController
{

    /**
     * @var BillingRepository
     */
    protected $repository;

    /**
     * @var BillingValidator
     */
    protected $validator;

    public function __construct(BillingRepository $repository, BillingValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);
        $type = $request->get('type','adv');
        $advertiser_id = $request->get('advertiser');

        $aff_id = $request->get('affiliate');

        $filter_month = $request->get('filter_month');

        if($type === 'aff'){
            $data = Billing::with([
                'affiliate'=>function($q){
                    $q->select('id','name');
                }
            ])->select([
                'customer',
                'date',
                \DB::raw('SUM(callback) as callback'),
                \DB::raw('SUM(revenue) revenue'),
                \DB::raw('SUM(payment) as payment'),
                \DB::raw('SUM(deduct) as deduct'),
                \DB::raw('SUM(pub_reduce) as pub_reduce'),
                \DB::raw('MIN(status) as status')
            ])->when($aff_id,function ($q) use($aff_id){
                $q->where('customer',$aff_id);
            })
                ->when($filter_month,function ($q) use($filter_month){
                    $q->where('date','=',$filter_month);
                })
                ->where('customer','!=',null)
                ->groupBy('customer','date')
                ->orderby('payment','desc')
                ->get();
        }else{
            $data = Billing::query()
                ->leftJoin('_tb_campaign as c','_tb_billing.campaign','=','c.id')
                ->leftJoin('_tb_advertiser as a','c.advertiser','=','a.id')
                ->select([
                    'date',
                    'a.id as id',
                    'a.name as name',
                    \DB::raw('SUM(_tb_billing.postback) as postback'),
                    \DB::raw('SUM(_tb_billing.revenue) revenue'),
                    \DB::raw('SUM(_tb_billing.payment) as payment'),
                    \DB::raw('SUM(_tb_billing.deduct) as deduct'),
                    \DB::raw('SUM(_tb_billing.pub_reduce) as pub_reduce'),
                    \DB::raw('MIN(_tb_billing.status) as status')
                ])
                ->when($advertiser_id,function ($q) use($advertiser_id){
                    $q->where('a.id',$advertiser_id);
                })
                ->when($filter_month,function ($q) use($filter_month){
                    $q->where('_tb_billing.date','=',$filter_month);
                })
                ->where('a.id','!=',null)
                ->groupBy('id','date')
                ->orderby('revenue','desc')
                ->get();

        }


        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $per_page, $per_page)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $per_page);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BillingCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BillingCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $billing = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $billing;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(BillingCreateRequest $request,$id)
    {
        //$id 广告住id
        $per_page = $request->get('paginationPerPage',20);
        $affiliate = $request->get('affiliate','');
        $offer_name = $request->get('offerName','');

        $month = $request->get('month');

        $data = Billing::with([
            'campaign'=>function($q){
                $q->select('id','name');
            },
            'affiliate'=>function($q){
                $q->select('id','name');
            }
        ])->whereHas('campaign',function ($q) use($id,$offer_name){
            $q->when($offer_name, function ($sub_query) use ($offer_name) {
                return $sub_query->where('name', 'like', '%' . $offer_name . '%');
            })->where('advertiser',$id);
        })->when($affiliate,function ($q) use($affiliate){
            $q->where('customer',$affiliate);
        })->where('date','=',$month)
            ->orderBy('id')
            ->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $per_page, $per_page)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $per_page);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;

    }


    /**
     * 显示affiliate的billing.
     *
     * @param  BillingUpdateRequest $request
     * @param  string            $id   ---> affiliate id
     *
     * @return Response
     */
    public function showAffiliate(BillingCreateRequest $request,$id)
    {
        $per_page = $request->get('paginationPerPage',20);
        $month = $request->get('month');
        $advertiser = $request->get('advertiser','');
        $offer_name = $request->get('offerName','');

        $data = Billing::with([
            'campaign'=>function($q){
                $q->select('id','advertiser','name');
            },
            'campaign.billing'=>function($q){
                $q->select('id','name');
            }
        ])->whereHas('campaign',function ($q) use($id,$advertiser,$offer_name){
            $q->when($advertiser, function ($sub_query) use ($advertiser) {
                return $sub_query->where('advertiser', $advertiser);
            })->when($offer_name, function ($sub_query) use ($offer_name) {
                return $sub_query->where('name', 'like', '%' . $offer_name . '%');
            });
        })->where([
            ['date','=',$month],
            ['customer','=',$id]
        ])
            ->orderBy('campaign')
            ->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $per_page, $per_page)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $per_page);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BillingUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(BillingUpdateRequest $request)
    {
        $type = $request->get('type');
        $ids = $request->get('ids');
        $data = $request->get('data');
        $status = $request->get('status');
        $adv_id = $request->get('adv_id');
        $month = $request->get('month');
        $aff_ids = $request->get('aff_ids');
        $deductFile = $request->get('deductFile');


        if($type === 'aff'){
            Billing::whereIn('id', $ids)->update($data);

            $this->updateAffInvoice($aff_ids,$month);

            return $this->response->noContent();
        }
        $cases = [];
        $ids = [];
        $params = [];

        foreach ($data as $item) {
            $id = $item['id'];
            $cases[] = "WHEN {$id} then ?";
            $params[] = $item['deduct'];
            $ids[] = $id;
        }
        foreach ($data as $item) {
            $params[] = $item['pub_reduce'];
        }
        foreach ($data as $item) {
            $params[] = $item['cover_amount'];
        }
        $ids = implode(',', $ids);
        $cases = implode(' ', $cases);
        $params[] = $status;

        \DB::update("UPDATE _tb_billing SET `deduct` = CASE `id` {$cases} END,`pub_reduce` = CASE `id` {$cases} END,`cover_amount` = CASE `id` {$cases} END, `status` = ? WHERE `id` in ({$ids})", $params);

        $this->updateAdvInvoice($adv_id,$month);
        $this->updateAffInvoice($aff_ids,$month);

        if(!!$deductFile['file_link']){ // 保存上传的核减报告
//            $deduct = Deduct::create([
//                "date" => $deductFile['date'],
//                "customer" => $deductFile['customer'],
//                "campaign" => $deductFile['campaign'],
//                "file_link" => $deductFile['file_link'],
//            ]);
//            $deduct->save();
            $deducts = [
                [
                    "date" => $deductFile['date'],
                    "customer" => $deductFile['customer'],
                    "campaign" => $deductFile['campaign'],
                    "file_link" => $deductFile['file_link']
                ]
            ];
            $updateColumns = ['file_link','created_at','updated_at'];

            $deduct = new Deduct();
            $deduct->batchInsertOrUpdate($deducts,'',[],$updateColumns);
        }

        return $this->response->noContent();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function updateAdvInvoice($id,$month)
    {
        $data=Advertiser::query()
            ->join('_tb_campaign as c',function ($q){
                return $q->on("c.advertiser",'=','_tb_advertiser.id');
            })
            ->join('_tb_billing as b',function ($q){
                return $q->on('b.campaign','=','c.id');
            })
            ->where([
                ['date','=',$month],
                ['_tb_advertiser.id','=',$id]
            ])
            ->groupBy('_tb_advertiser.id')
            ->select([
                '_tb_advertiser.id as id',
                \DB::raw('SUM(b.revenue) - SUM(b.deduct) as amount'),

            ])->get()->toArray();
        $data = $data[0];
        unset($data["id"]);
        Invoice::where([
            ['month','=',$month],
            ['advertiser','=',$id]
        ])->update($data);
    }
    public function updateAffInvoice($ids,$month)
    {
        $data=Billing::where('date','=',$month)
            ->whereIn('customer',$ids)
            ->groupBy('date','customer')
            ->select([
                'customer',
                \DB::raw('SUM(payment) - SUM(pub_reduce) + SUM(cover_amount) as amount'),

            ])->get()->toArray();

        $cases = [];
        $ids = [];
        $params = [];

        foreach ($data as $item) {
            $id = $item['customer'];
            $cases[] = "WHEN {$id} then ?";
            $params[] = $item['amount'];
            $ids[] = $id;
        }
        $ids = implode(',', $ids);
        $cases = implode(' ', $cases);

        \DB::update("UPDATE _tb_invoice SET `amount` = CASE `customer` {$cases} END WHERE `customer` in ({$ids}) and `month` = '{$month}'", $params);
    }
}
