<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\RelationCreateRequest;
use Someline\Http\Requests\RelationUpdateRequest;
use Someline\Models\Relation;
use Someline\Models\Campaign;
use Someline\Repositories\Interfaces\RelationRepository;
use Someline\Validators\RelationValidator;

class RelationsController extends BaseController
{

    /**
     * @var RelationRepository
     */
    protected $repository;

    /**
     * @var RelationValidator
     */
    protected $validator;

    public function __construct(RelationRepository $repository, RelationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->repository->paginate();
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RelationCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RelationCreateRequest $request)
    {
        $data = $request->get('data');

        $now = Carbon::now()->toDateTimeString();
        foreach ($data as $key => $item) {
            $item['created_at'] = $now;
            $data[$key] = $item;
        }

//        Relation::insertOrIgnore($data);
        $relation = new Relation();
        $relation->batchInsertOrUpdate($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
//        return $relation;
        return $this->response->noContent();

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RelationUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(RelationUpdateRequest $request, $campaign,$customer)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        Relation::where([
            ['campaign',$campaign],
            ['customer',$customer]
        ])->update($data);

        if(!!$data['pubx']){
            $array = array("pubkey" => 0);

            Campaign::where('id',$campaign)->update($array);
        }

        return $this->response->noContent();

    }


    public function changeStatus(RelationUpdateRequest $request)
    {
        $campaigns = $request->get('campaigns');
        $customers = $request->get('customers');
        $data = $request->get('data');
        Relation::whereIn('campaign', $campaigns)
            ->whereIn('customer',$customers)
            ->update($data);

        return $this->response->noContent();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function updatePubx(RelationUpdateRequest $request){
        $pubx = $request->get('pubx');
        $advertisers = $request->get('advertisers');


        Relation::query()
            ->leftJoin('_tb_campaign as c','_tb_relation.campaign','=','c.id')
            ->leftJoin('_tb_advertiser as a','a.id','=','c.advertiser')
            ->whereIn('a.id',$advertisers)
            ->update([
                'pubx' => $pubx,
            ]);
        return $this->response->noContent();
    }
}
