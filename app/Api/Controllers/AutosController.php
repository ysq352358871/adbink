<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\AutoCreateRequest;
use Someline\Http\Requests\AutoUpdateRequest;
use Someline\Repositories\Interfaces\AutoRepository;
use Someline\Validators\AutoValidator;
use Someline\Models\Auto;
use Uuid;

class AutosController extends BaseController
{

    /**
     * @var AutoRepository
     */
    protected $repository;

    /**
     * @var AutoValidator
     */
    protected $validator;

    public function __construct(AutoRepository $repository, AutoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);

        $adv = $request->get('adv');
        $aff = $request->get('aff');
        $jump = $request->get('jump');
        $package_name = $request->get('package_name');



        return $this->repository
            ->select([
                \DB::raw("group_concat(distinct advertiser separator ',') as advertiser"),
                \DB::raw("group_concat(distinct customer separator ',') as customer"),
                'package_name',
                'jump',
                'group_key',
                'group_name',
                \DB::raw('MIN(created_at) as created_at'),
                'daily'
            ])
            ->when($adv,function ($q) use($adv){
                $q->where('advertiser','=',$adv);
            })
            ->when($aff,function ($q) use($aff){
                $q->where('customer','=',$aff);
            })
            ->when($jump,function ($q) use($jump){
                $q->where('jump','=',$jump);
            })
            ->when($package_name,function ($q) use($package_name){
                $q->where('package_name','like','%'.$package_name."%");
            })
            ->groupBy('group_key')
            ->orderBy('id', 'desc')->paginate($per_page);
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AutoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AutoCreateRequest $request)
    {

        $data = $request->get('data');
        $group_key = $request->get('group_key','');
        $selected_adv = $request->get('selected_adv');
        $selected_aff = $request->get('selected_aff');

        if(!$group_key){
            $group_key = Uuid::generate()->string;
        }


        foreach ($data as $key => $item) {
            $item['group_key'] = $group_key;
            $data[$key] = $item;
        }
//        Auto::insertOrIgnore($data);
        $auto = new Auto();
        $auto->batchInsertOrUpdate($data);

        if(!!$group_key){
            Auto::where('group_key',$group_key)
                ->where(function ($q) use($selected_adv,$selected_aff){
                    $q->whereNotIn('advertiser',$selected_adv)
                        ->orWhereNotIn('customer',$selected_aff);
                })->delete();
        }

        return $this->response->noContent();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AutoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(AutoUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $auto = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(AutoUpdateRequest $request)
    {
        $keys = $request->get('keys');

//        $deleted = $this->repository->delete($id);
        $deleted = Auto::whereIn('group_key', $keys)->delete();
        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }
}
