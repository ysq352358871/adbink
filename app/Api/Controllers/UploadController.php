<?php


namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use Someline\Models\Billing;
use Someline\Models\Campaign;
use Someline\Services\OSS;
use Someline\Models\Invoice;
use Uuid;

class UploadController extends BaseController
{
    public function uploadAffInvoice(Request $request,$id)
    {
        //获取上传的文件
        $file = $request->file('file');

        //获取上传文件的临时地址
        $tmppath = $file->getRealPath();

        //生成文件名
        $fileName = rand(1000,9999) . $file->getFilename() . time() .date('ymd') . '.' . $file->getClientOriginalExtension();

        $pathName = 'invoice'.'/'.$fileName;

        //上传图片到阿里云OSS
        OSS::publicUpload(config('alioss.BucketName'), $pathName, $tmppath, ['ContentType' => $file->getClientMimeType()]);

        //获取上传文件的Url链接
        $Url = OSS::getPublicObjectURL(config('alioss.BucketName'), $pathName);
        $Url = str_replace("http://","https://",$Url);

        $invoice = Invoice::find($id);
        $invoice->aff_invoice_link = $Url;
        $invoice->save();

        return ['code' => 0, 'msg' => '上传成功', 'data' => ['src' => $Url]];
    }

    public function generateAdvInvoice(Request $request,$id)
    {
        $invoice_date = Carbon::now()->toDateTimeString();

        $invoice = $request->get('data');
        $adv = $invoice['advertiser'];
        $bank_tem = $request->get('bank_tem');
//        $adv = Advertiser::where('id',$id)->get(['id','name','address']);

        $view ='app.pdf.adv_invoice_'.$bank_tem;

        $invoice_no = 'INV-NO-'.Carbon::parse($invoice['month'])->format('Ym').'-'.$adv['id'];

        $invoice["invoice_no"] = $invoice_no;

        $file_name = $invoice_no.'.pdf'; //pdf name

        $full_path = storage_path('app/' . $file_name);

        $pdf = PDF::loadView($view,compact('adv','invoice','invoice_date'));

//        $file = $pdf->stream();

        $pdf->save($full_path);

        $pathName = 'invoice'.'/'.$file_name;

        //上传图片到阿里云OSS
        OSS::publicUpload(config('alioss.BucketName'), $pathName, $full_path, ['ContentType' => 'application/pdf']);
        //获取上传图片的Url链接
        $Url = OSS::getPublicObjectURL(config('alioss.BucketName'), $pathName);
        $Url = str_replace("http://","https://",$Url);

        $update_invoice = Invoice::find($invoice["id"]*1);
        $update_invoice->link = $Url;
        $update_invoice->invoice_date = $invoice_date;
        $update_invoice->save();

        // 删除storage下存的文件.
        \Storage::disk('local')->delete($file_name);

        return ['code' => 0, 'data' => ['src' => $Url]];
    }

    public function generateAffBilling(Request $request)
    {
        $invoice = $request->get('invoice');
        $platform = $invoice['affiliate']['platform'];
        $entity_name = 'Adbink Limited';

        if($platform === 'selectad' || $platform==='fan'){
            $entity_name = 'SELECTAD GROUP LIMITED';
        }
        $file_month= Carbon::parse($invoice["month"])->format('M_Y');

        /**
         * pdf name
         */
        $file_name = str_replace(' ','_',$entity_name).'_'.$file_month.'_'.time().'_billing';

        $full_path = storage_path('exports/' . $file_name.'.xlsx');

        $headers=['Offer Id','Offer Name','In Type','Total Conversion','Total Revenue','Reduce','Pending','Final Revenue','Validate'];

        $billings = Billing::query()->leftJoin('_tb_campaign AS c','_tb_billing.campaign','=','c.id')
            ->where([
            ['date','=',$invoice["month"]],
            ['customer','=',$invoice['customer']*1]
        ])
            ->select(
                'c.id as id',
                'c.name as name',
                \DB::raw("(CASE WHEN (c.api = 1) THEN 'API' ELSE '---' END) as api"),
                'callback',
                'payment',
                'pub_reduce',
                \DB::raw("(CASE WHEN (_tb_billing.status = 1) THEN 0 ELSE payment END) as pending"),
                \DB::raw('(CASE WHEN (_tb_billing.status = 1) THEN payment - pub_reduce ELSE 0 END) as final_revenue'),
                \DB::raw("(CASE WHEN (_tb_billing.status = 1) THEN 'Confirmed' ELSE 'Pending' END) as validate")
            )
            ->orderBy('campaign')
            ->get()->toArray();
        $cellData = array_merge([$headers], $billings);
        Excel::create($file_name,function($excel) use ($cellData){
            $excel->sheet('sheet1', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->store('xlsx');

        $pathName = 'billing'.'/'.$file_name.'.xlsx';

        //上传图片到阿里云OSS
        OSS::publicUpload(config('alioss.BucketName'), $pathName, $full_path, ['ContentType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']);

        //获取上传图片的Url链接
        $Url = OSS::getPublicObjectURL(config('alioss.BucketName'), $pathName);
        $Url = str_replace("http://","https://",$Url);

        $update_invoice = Invoice::find($invoice["id"]*1);
        $update_invoice->link = $Url;
        $update_invoice->save();

        // 删除storage下存的文件.
        \Storage::disk('exports')->delete($file_name.'.xlsx');

        return ['code' => 0, 'data' => ['src' => $Url]];
    }

    public function importBilling(Request $request)
    {
        Excel::load($request->file('file'), function($reader){
            $data = $reader->all()->toArray();
            $offers = assoc_unique($data,'offer');
            $insertOffers = [];
            foreach ($offers as $offer){
                if(!!$offer['offer']){
                    array_push($insertOffers,[
                        'name'     => $offer['offer'],
                        'advertiser'    => $offer['appone_adv'],
                        'today' => 0,
                        'lead' => 0,
                        'live' => 0,
                        'pull' => 0,
                        'status' => 0,
//                        'adv_oid' => $offer['advertiser_s_offer_id'],
                        'adv_oid' => Uuid::generate()->string,
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
            foreach ($insertOffers as $insertOffer){
                if(!!$insertOffer['name']){
                    $id = Campaign::insertGetId($insertOffer);
                    foreach ($data as $key => $item){
                        if($item['offer'] === $insertOffer['name']){
                            $item['offer_id'] = $id;
                            $data[$key] = $item;
                        }
                    }
                }
            }

            $insertBillings=[];
            foreach ($data as $insertBilling){
                if(!!$insertBilling['month'] && !!$insertBilling['offer_id'] && !!$insertBilling['pubs']){
                    array_push($insertBillings,[
                        'date'     => Carbon::parse($insertBilling['month'])->format('Y-m'),
                        'campaign'    => $insertBilling['offer_id'],
                        'customer' => $insertBilling['pubs'],
                        'postback' => $insertBilling['postback'],
                        'callback' => $insertBilling['callback'],
                        'revenue' => $insertBilling['revenueusd'],
                        'payment' => $insertBilling['payoutusd'],
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
            Billing::insertOrIgnore($insertBillings);

            $invoice_pubs = assoc_unique($data,'pubs');
            $invoice_advs = assoc_unique($data,'appone_adv');

            $insertInvoiceAdv = [];
            $insertInvoicePub = [];

            foreach ($invoice_advs as $adv){
                if(!!$adv['appone_adv']){
                    array_push($insertInvoiceAdv,[
                        'month'     => Carbon::parse($adv['month'])->format('Y-m'),
                        'advertiser'    => $adv['appone_adv'],
                        'customer' => 0,
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
            foreach ($invoice_pubs as $aff){
                if(!!$aff['pubs']){
                    array_push($insertInvoicePub,[
                        'month'     => Carbon::parse($aff['month'])->format('Y-m'),
                        'advertiser'    => 0,
                        'customer' => $aff['pubs'],
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }

            }
            $insertInvoices = array_merge($insertInvoiceAdv,$insertInvoicePub);
            Invoice::insertOrIgnore($insertInvoices);
        });
    }

    public function uploadDeduct(Request $request)
    {
//获取上传的文件
        $file = $request->file('file');
        $date = $request->get('date');
        $customer = $request->get('customer');
        $campaign = $request->get('campaign');

        //获取上传文件的临时地址
        $tmppath = $file->getRealPath();

        //生成文件名
        $fileName = 'Deduction_report_'.$date."_".$campaign."_".$customer."_". $file->getFilename() . time() .date('ymd') . '.' . $file->getClientOriginalExtension();

        $pathName = 'deduct'.'/'.$fileName;

        //上传图片到阿里云OSS
        OSS::publicUpload(config('alioss.BucketName'), $pathName, $tmppath, ['ContentType' => $file->getClientMimeType()]);

        //获取上传文件的Url链接
        $Url = OSS::getPublicObjectURL(config('alioss.BucketName'), $pathName);
        $Url = str_replace("http://","https://",$Url);


        return ['code' => 0, 'msg' => '上传成功', 'data' => ['src' => $Url]];
    }

}