<?php

namespace Someline\Api\Controllers;

use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\BusinessCreateRequest;
use Someline\Http\Requests\BusinessUpdateRequest;
use Someline\Repositories\Interfaces\BusinessRepository;
use Someline\Validators\BusinessValidator;
use Someline\Models\Business;
use Someline\Models\Schedule;

class BusinessesController extends BaseController
{

    /**
     * @var BusinessRepository
     */
    protected $repository;

    /**
     * @var BusinessValidator
     */
    protected $validator;

    public function __construct(BusinessRepository $repository, BusinessValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);

        return $this->repository->orderBy('top', 'desc')->paginate($per_page);
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BusinessCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BusinessCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $business = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $business;

    }


    public function createSchedule(BusinessCreateRequest $request,$id)
    {
        $business = Business::find($id);
        $data = $request->get('data');

        $business->schedules()->create([
            'contact' => $data['contact'],
            'name' => $data['name'],
            'mark' => $data['mark'],
        ]);

    }

    public function getSchedule($id)
    {
        $schedules = Schedule::with([
            'user',
        ])->where([
            ['scheduleable_id','=',$id],
            ['scheduleable_type','=',Business::TYPE]
        ])->get();

        return response()->json([
            'data' => $schedules
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BusinessUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(BusinessUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $business = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }
}
