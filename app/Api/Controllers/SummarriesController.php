<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\SummarryCreateRequest;
use Someline\Http\Requests\SummarryUpdateRequest;
use Someline\Models\Summarry;
use Someline\Models\Postback;
use Someline\Repositories\Interfaces\SummarryRepository;
use Someline\Validators\SummarryValidator;
use Illuminate\Database\Query\Builder as QBuilder;

class SummarriesController extends BaseController
{

    /**
     * @var SummarryRepository
     */
    protected $repository;

    /**
     * @var SummarryValidator
     */
    protected $validator;

    public function __construct(SummarryRepository $repository, SummarryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->repository->paginate();
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }


    public function getGeneralReport(SummarryCreateRequest $request)
    {
//        $sort = '%Y-%m';
//        $data = Summarry::with([
//            'campaign:id,name,advertiser',
//            'campaign.billing' => function ($querry) {
//                $querry->select('id','name');
//            },
//            'affiliate:id,name'
//        ])
//            ->whereBetween('date',[new Carbon('2020-12-18'),new Carbon('2020-12-22')])
//            ->groupBy(\DB::raw("date_format(date,'%Y-%m')"))
//            ->orderBy('date','desc')
//            ->get([
//                \DB::raw('SUM(postback) as postback'),
//                \DB::raw('SUM(tracking) as tracking'),
//                \DB::raw("date_format(date,'%Y-%m') as thedata"),
//                'campaign',
//                'customer'
//            ]);
        $perPage = $request->get('paginationPerPage',20);
        $advertiser = $request->get('advertiser');
        $offers = $request->get('offers');
        $package_name = $request->get('package_name');
        $affiliate = $request->get('affiliate');
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];
        $aff_pub = (int)$request->input('aff_pub',0);
        $search_aff_pub = $request->input('search_aff_pub','');
        $search_aff_key = $request->input('search_aff_key','');
        $show_daily = (int)$request->input('show_daily',0);
        $filed = $request->input('filed','revenue');
        $sort_type = $request->input('sort_type','desc');
        $geo = $request->input('geo','');

        $group = array( 'campaign','customer');

        $getFiled=array(
            \DB::raw('SUM(revenue) as revenue'),
            \DB::raw('SUM(payment) as payment'),
            'campaign',
            'customer',
        );
        if(!!$show_daily){
            array_push($group,'date');
            array_push($getFiled,\DB::raw("date_format(date,'%Y-%m-%d') as date"));
        }

        if(!!$aff_pub){
            array_push($group,'aff_pub');
            array_push($getFiled,\DB::raw('count(*) as postback'),'aff_pub','aff_key','table_a.callback as callback');
            $temp_a=Postback::where('take','=',0)
                ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
                ->where('aff_pub', '!=', '')
                ->selectRaw('campaign as p_campaign,customer as p_customer,aff_pub as p_aff_pub,count(take) as callback,date as p_date');
            if(!!$show_daily){
                $temp_a = $temp_a->groupBy('p_campaign','p_customer','p_aff_pub','p_date');
            }else{
                $temp_a = $temp_a->groupBy('p_campaign','p_customer','p_aff_pub');
            }

            $query = Postback::with([
                'campaign' => function($query){
                    $query->select('id','name','advertiser','package_name','geo');
                },
                'campaign.billing' => function ($query){
                    $query->select('id','name');
                },
                'affiliate' => function($query){
                    $query->select('id','name');
                }
            ])
                ->leftJoinSub($temp_a,'table_a',function(QBuilder $query) use ($aff_pub){
                    $q = $query->on('_tb_postback.campaign','=','table_a.p_campaign')
                        ->on('_tb_postback.customer','=','table_a.p_customer')
                        ->on('_tb_postback.aff_pub','=','table_a.p_aff_pub');
                    if(!!$aff_pub){
                        $q =$query->on('_tb_postback.date','=','table_a.p_date');
                    }
                    return $q;
                })
                ->where('aff_pub', '!=', '');
        }else{
            array_push($getFiled,\DB::raw('SUM(postback) as postback'),\DB::raw('SUM(callback) as callback'),\DB::raw('SUM(tracking) as tracking'));
            $query = Summarry::with([
                'campaign' => function($query){
                    $query->select('id','name','advertiser','package_name','geo');
                },
                'campaign.billing' => function ($query){
                    $query->select('id','name');
                },
                'affiliate' => function($query){
                    $query->select('id','name');
                }
            ]);
        }


        $query->when($advertiser, function ($query) use ($advertiser) {
                    return $query->advertiser($advertiser);
                })
                ->when($offers, function ($query) use ($offers) {
                    $query->whereHas('campaign', function ($sub_query) use ($offers) {
                        $sub_query->whereIn('id',$offers);
                    });
                })
                ->when($package_name, function ($query) use ($package_name) {
                    $query->whereHas('campaign', function ($sub_query) use ($package_name) {
                        $sub_query->where('package_name', 'like', '%' . $package_name . '%');
                    });
                })
                ->when($geo, function ($query) use ($geo) {
                    $query->whereHas('campaign', function ($sub_query) use ($geo) {
                        $sub_query->where('geo', 'like', '%' . $geo . '%');
                    });
                })
                ->when($affiliate, function ($query) use ($affiliate) {
                    $query->where('customer',$affiliate);
                })
                ->when($search_aff_pub, function ($query) use ($search_aff_pub) {
                    $query->where('aff_pub','=',$search_aff_pub);
                })
                ->when($search_aff_key, function ($query) use ($search_aff_key) {
                    $query->where('aff_key','like','%'.$search_aff_key.'%');
                })
                ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);
            $data = $query->groupBy($group)
                ->orderBy($filed,$sort_type)
                ->select($getFiled)->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;

//        $data = Campaign::with(['summarries' => function($query) {
//            $query->groupBy('date','campaign')
//                ->orderBy('date','desc')
//                ->select('date', 'campaign', 'customer',\DB::raw('SUM(postback) as postback'));
//        }])->get()->toArray();
//        return $data;
    }

    public function getDailyData(Request $request)
    {
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];
        $user_id = $request->get('user_id');

        if(!$user_id){
            $results = Summarry::whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"), [new Carbon($start_date),new Carbon($end_date)])
                ->groupBy('date')
                ->orderBy('date')
                ->get([
                    \DB::raw('DATE_FORMAT(date, "%Y-%m-%d") as date'),
                    \DB::raw('SUM(postback) as postback'),
                    \DB::raw('SUM(tracking) as tracking'),
                    \DB::raw('SUM(revenue) as revenue'),
                    \DB::raw('SUM(payment) as payment'),
                    \DB::raw('SUM(revenue) - SUM(payment) as profit'),
                ])
                ->keyBy('date')
                ->map(function ($item) {
                    $item->date = Carbon::parse($item->date)->format('Y-m-d');
                    return $item;
                });
        }else{
            $results = Summarry::whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"), [new Carbon($start_date),new Carbon($end_date)])
                ->whereHas('campaign.billing.user',function ($sub_query) use ($user_id) {
                    $sub_query->where('user_id', $user_id);
                })
                ->groupBy('date')
                ->orderBy('date')
                ->get([
                    \DB::raw('DATE_FORMAT(date, "%Y-%m-%d") as date'),
                    \DB::raw('SUM(postback) as postback'),
                    \DB::raw('SUM(tracking) as tracking'),
                    \DB::raw('SUM(revenue) as revenue'),
                    \DB::raw('SUM(payment) as payment'),
                    \DB::raw('SUM(revenue) - SUM(payment) as profit'),
                ])
                ->keyBy('date')
                ->map(function ($item) {
                    $item->date = Carbon::parse($item->date)->format('Y-m-d');
                    return $item;
                });
        }


        $period = new \DatePeriod(new Carbon($start_date), CarbonInterval::day(), (new Carbon($end_date))->addDay());

        $data = array_map(function ($datePeriod) use ($results) {
            $date = $datePeriod->format('Y-m-d');
            if($results->has($date)){
                return $results->get($date);
            }
            return array(
                'date'=>$date,
                'postback'=>0,
                'tracking'=>0,
                'revenue'=>0,
                'profit'=>0,
                'payment'=>0
            );

        }, iterator_to_array($period));

        return response()->json([
            'data' => $data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SummarryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SummarryCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $summarry = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $summarry;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  SummarryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(SummarryUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $summarry = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }
}
