<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Someline\Models\Deduct;
use Someline\Models\Relation;
use Uuid;
use Carbon\CarbonInterval;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\AffiliateCreateRequest;
use Someline\Http\Requests\AffiliateUpdateRequest;
use Illuminate\Support\Facades\Mail;
use Someline\Models\Affiliate;
use Someline\Models\Campaign;
use Someline\Models\Summarry;
use Someline\Models\Invoice;
use Someline\Mail\AffiliateBilling;
use Someline\Repositories\Interfaces\AffiliateRepository;
use Someline\Validators\AffiliateValidator;

class AffiliatesController extends BaseController
{

    /**
     * @var AffiliateRepository
     */
    protected $repository;

    /**
     * @var AffiliateValidator
     */
    protected $validator;

    public function __construct(AffiliateRepository $repository, AffiliateValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('paginationPerPage',20);

        $traffic_type = $request->get('traffic_type','');

        return $this->repository
            ->when($traffic_type,function ($q) use($traffic_type){
                $q->where('traffic_type','like','%'.$traffic_type.'%');
            })
            ->orderBy('id', 'desc')->paginate($per_page);
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    public function top(Request $request)
    {
        $top = $request->get('top',50);
        $id = $request->get('id'); // offer的id. 如果有: 查询某个offer下的top渠道
        $user_id = $request->get('user_id'); // 查询某个用户下的top渠道。

        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $query = Affiliate::query()
            ->leftJoin('_tb_summarry AS s','_tb_customer.id','=','s.customer')
            ->whereBetween(\DB::raw("date_format(s.date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);

        if($user_id){
            $query = $query->leftJoin('_tb_campaign as c','s.campaign','=','c.id')
                ->leftJoin('_tb_advertiser as a','c.advertiser','=','a.id')
                ->where('a.user_id',$user_id);
        }

        if($id){
            $query = $query->when($id,function ($q) use ($id){
                $q->where('s.campaign',$id);
            });
        }

        $data = $query->select(
                '_tb_customer.id as id',
                '_tb_customer.name as name',
                \DB::raw('SUM(s.tracking) as tracking'),
                \DB::raw('SUM(s.postback) as postback'),
                \DB::raw('SUM(s.callback) as callback'),
                \DB::raw('SUM(s.revenue) as revenue'),
                \DB::raw('SUM(s.payment) as payment'),
                \DB::raw('SUM(s.revenue) - SUM(payment) as profit')
            )
            ->groupBy('id')
            ->orderBy('revenue','desc')
            ->take($top)
            ->get();

        return response()->json([
            'data' => $data
        ]);
    }


    public function topOffers(Request $request,$id)
    {
        $top = $request->get('top',50);

        $user_id = $request->get('user_id');

        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $query = Campaign::query();

        if($user_id){
            $query = $query ->leftJoin('_tb_advertiser AS a','_tb_campaign.advertiser','=','a.id');
        }

        $data = $query->leftJoin('_tb_summarry AS s','_tb_campaign.id','=','s.campaign')
            ->whereBetween(\DB::raw("date_format(s.date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
            ->when($user_id,function ($q) use ($user_id){
                $q->where('a.user_id',$user_id);
            })
            ->where('s.customer',$id)
            ->select(
                '_tb_campaign.id as id',
                '_tb_campaign.name as name',
                \DB::raw('SUM(s.tracking) as tracking'),
                \DB::raw('SUM(s.postback) as postback'),
                \DB::raw('SUM(s.callback) as callback'),
                \DB::raw('SUM(s.revenue) as revenue'),
                \DB::raw('SUM(s.payment) as payment'),
                \DB::raw('SUM(s.revenue) - SUM(payment) as profit')
            )
            ->groupBy('id')
            ->orderBy('revenue','desc')
            ->take($top)
            ->get();


        return response()->json([
            'data' => $data
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  AffiliateCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AffiliateCreateRequest $request)
    {

        $data = $request->all();
        $password = $data['password'];
        if(!!$password){
            $data['password'] = \Hash::make($password);
        }else{
            unset($data['password']);
        }



        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $affiliate = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $affiliate;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AffiliateUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(AffiliateUpdateRequest $request, $id)
    {

        $data = $request->all();

        $password = $data['password'];
        if(!!$password){
            $data['password'] = \Hash::make($password);
        }else{
            unset($data['password']);
        }

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $affiliate = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }

    public function updateKey(AffiliateUpdateRequest $request, $id)
    {
        $api_key = Uuid::generate()->string;

        $aff = Affiliate::find($id);

        $aff->api_key = $api_key;

        $aff->save();

        return $api_key;
    }


    public function getRelationOffers(AffiliateUpdateRequest $request,$id)
    {
        $perPage = (int)$request->get('paginationPerPage',20);

        $is_aff = (boolean)$request->get('is_aff',false); // 是否从渠道端获取渠道的offer

        $search = $request->get('search','');

        $query = Affiliate::query()
            ->join('_tb_relation as r',function ($q){
                return $q->on('r.customer','=','_tb_customer.id');
            })
            ->join('_tb_campaign as c',function ($q){
                return $q->on('c.id','=','r.campaign');
            });
        $select = array(
            'r.campaign as campaign',
            'r.customer as customer',
            'c.id as id',
            'c.name as name',
            'a.margin as adv_margin',
            'c.margin as off_margin',
            'r.margin as actual_margin',
            'c.payout as payout',
            'r.price as price',
            'c.paytype as paytype',
            'r.live as live',
            'r.status as status',
            'r.daily as daily',
            'r.pubx as pubx'
        );
        $query->join('_tb_advertiser as a',function ($q){
            return $q->on('a.id','=','c.advertiser');
        });

        $campaigns = $query->where('_tb_customer.id',$id)->select($select)->get();
        // 手动分页
        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $campaigns->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $campaigns->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return response()->json([
            'data' => $paginatedResult,
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function destroyAll($id)
    {
        $deleted = Relation::where('customer',$id)->delete();
        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }


    public function getCustomerOffers(Request $request,$id)
    {
        $search = $request->get('search');

        $perPage = $request->get('paginationPerPage',20);

        $geo =  $request->get('geo');

        $data = Campaign::with([
            'applies'=>function($q) use ($id){
                $q->where('customer_id',$id)
                    ->select('id','campaign_id','status','updated_at','customer_id');
            }
        ])
            ->leftJoin('_tb_relation',function ($q) use ($id){
                $q->on('_tb_campaign.id','=','_tb_relation.campaign')
                    ->where('_tb_relation.customer',$id);
            })
            ->select(
                'name','_tb_campaign.id as id','geo','package_name','os','_tb_campaign.price as price',
                '_tb_campaign.daily as daily','_tb_campaign.live as live',
                '_tb_campaign.status as status'
            )
            ->where(function ($query) use ($id){
                $query->where('_tb_relation.customer',$id)
                    ->orWhereHas('applies', function($query) use ($id){
                        $query->where('customer_id', $id);

                    });
            })
            ->when($geo, function ($sub_query) use ($geo) {
                return $sub_query->where('geo', 'like', '%' . $geo . '%');
            })
            ->when($search, function ($sub_query) use ($search) {
                return $sub_query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('package_name', 'like', '%' . $search . '%');
            })
            ->orderBy('id', 'desc')->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    public function getCustomerTops(Request $request,$id)
    {
        $top = $request->get('top',20);

        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $query = Campaign::query();


        $data = $query->leftJoin('_tb_summarry AS s','_tb_campaign.id','=','s.campaign')
            ->whereBetween(\DB::raw("date_format(s.date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
            ->where('s.customer',$id)
            ->select(
                '_tb_campaign.id as id',
                '_tb_campaign.name as name',
                \DB::raw('SUM(s.payment) as revenue')
            )
            ->groupBy('id')
            ->orderBy('revenue','desc')
            ->take($top)
            ->get();


        return response()->json([
            'data' => $data
        ]);
    }

    public function getCustomerGraph(Request $request,$id)
    {
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $results = Summarry::whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"), [new Carbon($start_date),new Carbon($end_date)])
            ->where('customer',$id)
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                \DB::raw('DATE_FORMAT(date, "%Y-%m-%d") as date'),
                \DB::raw('SUM(callback) as callback'),
                \DB::raw('SUM(tracking) as tracking'),
                \DB::raw('SUM(payment) as profit')
            ])
            ->keyBy('date')
            ->map(function ($item) {
                $item->date = Carbon::parse($item->date)->format('Y-m-d');
                return $item;
            });
        $period = new \DatePeriod(new Carbon($start_date), CarbonInterval::day(), (new Carbon($end_date))->addDay());

        $data = array_map(function ($datePeriod) use ($results) {
            $date = $datePeriod->format('Y-m-d');
            if($results->has($date)){
                return $results->get($date);
            }
            return array(
                'date'=>$date,
                'callback'=>0,
                'tracking'=>0,
                'profit'=>0
            );

        }, iterator_to_array($period));

        return response()->json([
            'data' => $data
        ]);
    }


    public function getCustomerReport(Request $request,$id)
    {
        $perPage = $request->get('paginationPerPage',20);
        $offers = $request->get('offers');
        $package_name = $request->get('package_name');
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $group = array( 'campaign','customer');

        $getFiled=array(
            \DB::raw('SUM(payment) as payment'),
            \DB::raw('SUM(tracking) as tracking'),
            \DB::raw('SUM(callback) as callback'),
            'campaign',
        );
        $query = Summarry::with([
            'campaign' => function($query){
                $query->select('id','name','package_name');
            }
        ]);
        $query->where('customer',$id)
            ->when($offers, function ($query) use ($offers) {
                $query->whereHas('campaign', function ($sub_query) use ($offers) {
                    $sub_query->whereIn('id',$offers);
                });
            })
            ->when($package_name, function ($query) use ($package_name) {
                $query->whereHas('campaign', function ($sub_query) use ($package_name) {
                    $sub_query->where('package_name', 'like', '%' . $package_name . '%');
                });
            })
            ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);
        $data = $query->groupBy($group)
            ->orderBy('date','desc')
            ->select($getFiled)->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    public function sendBillingEmail(AffiliateUpdateRequest $request,$id)
    {
        $invoice = $request->get('invoice');
        $files = Deduct::where([
            ['date','=',$invoice['month']],
            ['customer','=',$invoice['customer']]
        ])->pluck('file_link');

        Mail::to($invoice['affiliate']['email'])
            ->cc('support@appone.link')
            ->send(new AffiliateBilling($invoice,$files));

        $email_send_time = Carbon::now()->toDateTimeString();
        $update_invoice = Invoice::find($invoice["id"]*1);
        $update_invoice->email_send_time = $email_send_time;
        $update_invoice->save();

        return [
            "code"=>0,
            "data"=>$email_send_time
        ];
    }
}
