<?php


namespace Someline\Http\Controllers;

use Illuminate\Http\Request;
use Someline\Models\Campaign;
use Someline\Models\Country;
use Uuid;


class CampaignController extends BaseController
{

    public function show($id)
    {
        $offer = Campaign::find($id);

//        dd($offer->affiliates[0]->pivot->price);
        $offer->billing;

        $offer->geo = Country::whereIn('code',explode(',',$offer->geo))->pluck('country');

        return view('app.offers.show', [
            'offer_id' => $id,
            'offer'=>$offer
        ]);
    }

    public function create(Request $request)
    {
        $advOid = Uuid::generate()->string;

        $offer_id = $request->get('offer_id','');

        return view('app.offers.create', [
            'advOid' => $advOid,
            'offer_id' => $offer_id
        ]);
    }

    public function getCampaignEdit($id)
    {
        $offer = Campaign::find($id);
        return view('app.offers.edit', [
            'offer_id' => $id,
            'offer'=>$offer
        ]);
    }
}