<?php


namespace Someline\Http\Controllers;

use Carbon\Carbon;
use Someline\Models\Invoice;
use Someline\Models\Summarry;

class InvoiceController extends BaseController
{
    public function generateInvoice()
    {

        $start = new Carbon('first day of last month');

        $start_date = $start->toDateString();

        $end = new Carbon('last day of last month');

        $end_date = $end->toDateString();

        $getAffFiled=array(
            \DB::raw("date_format(date,'%Y-%m') as month"),
            'customer',
            \DB::raw('SUM(payment) as amount')
        );

        $aff_data = Summarry::whereBetween(\DB::raw("date_format(date,'%Y-%m-%d')"),[$start_date,$end_date])
            ->where('postback','>',0)
            ->groupBy('customer',\DB::raw("date_format(date,'%Y-%m')"))
            ->orderby('customer','desc')
            ->select($getAffFiled)->get()->toArray();

        foreach ($aff_data as $key =>$item){
            $item['advertiser'] = 0;
            $aff_data[$key] = $item;
        }

        $adv_data = Summarry::whereBetween(\DB::raw("date_format(date,'%Y-%m-%d')"),[$start_date,$end_date])
            ->leftJoin('_tb_campaign as c','_tb_summarry.campaign','=','c.id')
            ->leftJoin('_tb_advertiser as a','c.advertiser','a.id')
            ->select([
                \DB::raw("date_format(_tb_summarry.date,'%Y-%m') as month"),
                'c.advertiser as advertiser',
                \DB::raw('SUM(_tb_summarry.revenue) as amount')
            ])
            ->where('_tb_summarry.postback','>',0)
            ->groupBy('advertiser','month')
            ->orderby('advertiser','desc')
            ->get()->toArray();

        foreach ($adv_data as $key =>$item){
            $item['customer'] = 0;
            $adv_data[$key] = $item;
        }

        $data = array_merge($adv_data,$aff_data);


        Invoice::insertOrIgnore($data);
    }

}