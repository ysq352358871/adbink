<?php

namespace Someline\Http\Controllers;
use Mail;
use Someline\Models\Foundation\User;

class UserController extends BaseController
{

    public function all()
    {
        $users = User::all();

        return view('app.users.index', [
            'users'=>$users
        ]);
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('app.users.profile', [
            'user' => $user,
        ]);
    }

    public function send()
    {
        Mail::raw('你好，我是PHP程序！', function ($message) {
            $to = 'shengqian.yang@compasslist.com';
            $message ->to($to)->subject('纯文本信息邮件测试');
        });

        dd(Mail::failures());
    }

}