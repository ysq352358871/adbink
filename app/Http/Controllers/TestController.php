<?php


namespace Someline\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder as QBuilder;
use Illuminate\Pagination\LengthAwarePaginator;
use PDF;
use Someline\Models\Advertiser;
use Someline\Models\Billing;
use Maatwebsite\Excel\Facades\Excel;
use Someline\Models\Campaign;
use Someline\Models\Invoice;
use Someline\Models\Relation;
use Someline\Http\Requests\SummarryCreateRequest;
use Someline\Models\Postback;
use Someline\Models\Summarry;
use Someline\Models\Outlay;

class TestController
{
    public function testPdf()
    {
        $file_name = 'invoice-CompassList.pdf';

        $full_path = storage_path('app/' . $file_name);
//        $pdf = PDF::loadView('app.pdf.adv_invoice_2');
        return View('app.pdf.test');
//        return $pdf->stream();
//        return $pdf->save($full_path)->download($file_name);
//        return $pdf->download($file_name);
        return $pdf->inline();
    }
    public function testAffPdf()
    {
        $file_name = 'invoice-CompassList.pdf';

        $full_path = storage_path('app/' . $file_name);
        $pdf = PDF::loadView('app.pdf.aff_billing')->setPaper('a4', 'landscape');
//        return View('app.pdf.aff_billing');
        return $pdf->stream();
//        return $pdf->save($full_path)->download($file_name);
    }

    public function testUpdateInvoice()
    {
        $month = '2020-12';
        $id=1;
        $data=Advertiser::query()
            ->join('_tb_campaign as c',function ($q){
                return $q->on("c.advertiser",'=','_tb_advertiser.id');
            })
            ->join('_tb_billing as b',function ($q){
                return $q->on('b.campaign','=','c.id');
            })
            ->where([
                ['b.date','=',$month],
                ['_tb_advertiser.id','=',$id]
            ])
            ->groupBy('_tb_advertiser.id')
            ->select([
                '_tb_advertiser.id as id',
                \DB::raw('SUM(b.revenue) - SUM(b.deduct) as amount'),

            ])->get()->toArray();
        dd($data[0]);
    }
    public function testUpdateAffInvoice()
    {
        $month = '2020-12';
        $ids=array(80000);
        $data=Billing::where('date','=',$month)
            ->whereIn('customer',$ids)
            ->groupBy('date','customer')
            ->select([
                'customer',
                \DB::raw('SUM(payment) - SUM(pub_reduce) as amount'),

            ])->get()->toArray();
        dd($data);
        $cases = [];
        $ids = [];
        $params = [];

        foreach ($data as $item) {
            $id = $item['customer'];
            $cases[] = "WHEN {$id} then ?";
            $params[] = $item['amount'];
            $ids[] = $id;
        }
        $ids = implode(',', $ids);
        $cases = implode(' ', $cases);

        \DB::update("UPDATE _tb_invoice SET `amount` = CASE `customer` {$cases} END WHERE `customer` in ({$ids}) and `month` = '{$month}'", $params);
    }

    public function importBilling()
    {
        $cretae_path = base_path().'/'.'test.xlsx';

        Excel::load($cretae_path, function($reader){
            $data = $reader->all()->toArray();
            $offers = assoc_unique($data,'offer');
            $insertOffers = [];
            foreach ($offers as $offer){
                if(!!$offer['offer']){
                    array_push($insertOffers,[
                        'name'     => $offer['offer'],
                        'advertiser'    => $offer['appone_adv'],
                        'today' => 0,
                        'lead' => 0,
                        'live' => 0,
                        'pull' => 0,
                        'status' => 0,
                        'adv_oid' => $offer['advertiser_s_offer_id'],
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
            foreach ($insertOffers as $insertOffer){
                if(!!$insertOffer['name']){
                    $id = Campaign::insertGetId($insertOffer);
                    foreach ($data as $key => $item){
                        if($item['offer'] === $insertOffer['name']){
                            $item['offer_id'] = $id;
                            $data[$key] = $item;
                        }
                    }
                }
            }

            $insertBillings=[];
            foreach ($data as $insertBilling){
                if(!!$insertBilling['month'] && !!$insertBilling['offer_id'] && !!$insertBilling['pubs']){
                    array_push($insertBillings,[
                        'date'     => Carbon::parse($insertBilling['month'])->format('Y-m'),
                        'campaign'    => $insertBilling['offer_id'],
                        'customer' => $insertBilling['pubs'],
                        'postback' => $insertBilling['postback'],
                        'callback' => $insertBilling['callback'],
                        'revenue' => $insertBilling['revenueusd'],
                        'payment' => $insertBilling['payoutusd'],
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
            Billing::insertOrIgnore($insertBillings);

            $invoice_pubs = assoc_unique($data,'pubs');
            $invoice_advs = assoc_unique($data,'appone_adv');

            $insertInvoiceAdv = [];
            $insertInvoicePub = [];

            foreach ($invoice_advs as $adv){
                if(!!$adv['appone_adv']){
                    array_push($insertInvoiceAdv,[
                        'month'     => Carbon::parse($adv['month'])->format('Y-m'),
                        'advertiser'    => $adv['appone_adv'],
                        'customer' => 0,
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
            foreach ($invoice_pubs as $aff){
                if(!!$aff['pubs']){
                    array_push($insertInvoicePub,[
                        'month'     => Carbon::parse($aff['month'])->format('Y-m'),
                        'advertiser'    => 0,
                        'customer' => $aff['pubs'],
                        'created_at' => Carbon::now()->toDateTimeString()
                    ]);
                }
            }
            $insertInvoices = array_merge($insertInvoiceAdv,$insertInvoicePub);
            Invoice::insertOrIgnore($insertInvoices);
        });
    }

    public function exportReport(SummarryCreateRequest $request)
    {
        $headers=['Offer name','Date','Advertiser','Affiliate','Sub-affiliate','Sub-security','Package Name','Postback','Callback','Revenue','Payment'];
        $advertiser = $request->get('advertiser');
        $offers = '';
        $package_name = '';
        $affiliate = '';
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];
        $aff_pub = 1;
        $search_aff_pub = $request->input('search_aff_pub','');
        $search_aff_key = $request->input('search_aff_key','');
        $show_daily = (int)$request->input('show_daily',1);
        $filed = $request->input('filed','revenue');
        $sort_type = $request->input('sort_type','desc');

        $group = array( 'campaign','customer','date','aff_pub');

        $getFiled=array(
            \DB::raw("CONCAT(c.name,'#',c.id) as campaign_name"),
            \DB::raw("date_format(date,'%Y-%m-%d') as date"),
            \DB::raw("CONCAT(a.name,'#',a.id) as advertiser_name"),
            \DB::raw("CONCAT(cu.name,'#',cu.id) as customer_name"),
            'aff_pub',
            'aff_key',
            'c.package_name as package_name',
            \DB::raw('count(*) as postback'),
            'table_a.callback as callback',
            \DB::raw('SUM(revenue) as revenue'),
            \DB::raw('SUM(payment) as payment')
        );
        $temp_a=Postback::where('take','=',0)
            ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
            ->where('aff_pub', '!=', '')
            ->selectRaw('campaign as p_campaign,customer as p_customer,aff_pub as p_aff_pub,count(take) as callback,date as p_date');
        if(!!$show_daily){
            $temp_a = $temp_a->groupBy('p_campaign','p_customer','p_aff_pub','p_date');
        }else{
            $temp_a = $temp_a->groupBy('p_campaign','p_customer','p_aff_pub');
        }

        $query = Postback::query()
            ->leftJoin('_tb_campaign AS c','_tb_postback.campaign','=','c.id')
            ->leftJoin('_tb_advertiser AS a','c.advertiser','=','a.id')
            ->leftJoin('_tb_customer AS cu','_tb_postback.customer','=','cu.id')
            ->leftJoinSub($temp_a,'table_a',function(QBuilder $query) use ($aff_pub){
                $q = $query->on('_tb_postback.campaign','=','table_a.p_campaign')
                    ->on('_tb_postback.customer','=','table_a.p_customer')
                    ->on('_tb_postback.aff_pub','=','table_a.p_aff_pub');
                if(!!$aff_pub){
                    $q =$query->on('_tb_postback.date','=','table_a.p_date');
                }
                return $q;
            })
            ->where('aff_pub', '!=', '');


        $query->when($advertiser, function ($query) use ($advertiser) {
            return $query->advertiser($advertiser);
        })
            ->when($offers, function ($query) use ($offers) {
                $query->whereHas('campaign', function ($sub_query) use ($offers) {
                    $sub_query->whereIn('id',$offers);
                });
            })
            ->when($package_name, function ($query) use ($package_name) {
                $query->whereHas('campaign', function ($sub_query) use ($package_name) {
                    $sub_query->where('package_name', 'like', '%' . $package_name . '%');
                });
            })
            ->when($affiliate, function ($query) use ($affiliate) {
                $query->where('customer',$affiliate);
            })
            ->when($search_aff_pub, function ($query) use ($search_aff_pub) {
                $query->where('aff_pub','=',$search_aff_pub);
            })
            ->when($search_aff_key, function ($query) use ($search_aff_key) {
                $query->where('aff_key','like','%'.$search_aff_key.'%');
            })
            ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);
        $data = $query->groupBy($group)
            ->orderBy($filed,$sort_type)
            ->select($getFiled)->get()->toArray();
        $cellData = array_merge([$headers], $data);
        return Excel::create('test',function($excel) use ($cellData){
            $excel->sheet('sheet1', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->download('xlsx');
    }

    public function test(){
        // 在需要打印SQL的语句前添加监听事件。
        \DB::listen(function($query) {
            $bindings = $query->bindings;
            $sql = $query->sql;
            foreach ($bindings as $replace){
                $value = is_numeric($replace) ? $replace : "'".$replace."'";
                $sql = preg_replace('/\?/', $value, $sql, 1);
            }
            dd($sql);
        });


        $data = Relation::query()
            ->leftJoin('_tb_campaign as c','_tb_relation.campaign','=','c.id')
            ->leftJoin('_tb_advertiser as a','a.id','=','c.advertiser')
            ->where('a.id',1)
            ->update([
                'pubx' => 1,
            ])->tosql();
    }
    public function getTotal()
    {
//        ->selectRaw('_tb_invoice.month as month,,SUM(pay_amount) as pay_amount')
        $month = '2020-12';
        $temp_advertiser = Invoice::query()
            ->where([
                ['month','=',$month],
                ['advertiser','>',0]
            ])
            ->groupBy('month')->selectRaw('month as advertiser_month,SUM(amount) as final_revenue,SUM(pay_amount) as pay_revenue');
        $temp_other = Outlay::query()
            ->where('month',$month)
            ->groupBy('month')
            ->selectRaw("month as other_month,CAST(SUM(case when currency = 'cny' then amount/6.5 else amount end) as DECIMAL(18,3)) as other_amount")
            ->getQuery();
        $data = Invoice::query()
            ->where([
                ['month','=',$month],
                ['customer','>',0]
            ])
            ->groupBy('month')
            ->joinSub($temp_advertiser, 'advertiser', 'advertiser.advertiser_month', '=', '_tb_invoice.month')
            ->joinSub($temp_other, 'other', 'other.other_month', '=', '_tb_invoice.month')
            ->get([
                '_tb_invoice.month as month',
                'final_revenue',
                'pay_revenue',
                \DB::raw('SUM(amount) as final_amount'),
                \DB::raw('SUM(pay_amount) as pay_amount'),
                'other_amount'
            ]);

        return $data;
    }
}