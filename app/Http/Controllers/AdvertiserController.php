<?php


namespace Someline\Http\Controllers;


use Someline\Models\Advertiser;

class AdvertiserController
{

    public function show($id)
    {
        $advertiser = Advertiser::find($id);
        return view('app.advertisers.show', [
            'advertiser_id' => $id,
            'advertiser'=>$advertiser
        ]);
    }

    public function getAdvertiserEdit($id)
    {
        $advertiser = Advertiser::find($id);
        return view('app.advertisers.edit', [
            'advertiser_id' => $id,
            'advertiser'=>$advertiser
        ]);
    }

}