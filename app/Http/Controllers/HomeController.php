<?php

namespace Someline\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder as QBuilder;
use Illuminate\Http\Request;
use Someline\Http\Requests\PostbackCreateRequest;
use Someline\Http\Requests\SummarryCreateRequest;
use Someline\Models\Postback;
use Maatwebsite\Excel\Facades\Excel;
use Someline\Models\Summarry;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function exportReport(SummarryCreateRequest $request)
    {
        $headers=['Offer name','Advertiser','Affiliate','Package Name','Revenue','Payment'];

        $advertiser = $request->get('advertiser');
        $offers = $request->get('offers','');

        if(!!$offers){
            $offers = explode(',',$offers);
        }
        $package_name = $request->get('package_name');
        $affiliate = $request->get('affiliate');
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];
        $aff_pub = (int)$request->input('aff_pub',0);
        $search_aff_pub = $request->input('search_aff_pub','');
        $search_aff_key = $request->input('search_aff_key','');
        $show_daily = (int)$request->input('show_daily',0);
        $filed = $request->input('filed','revenue');
        $sort_type = $request->input('sort_type','desc');
        $geo = $request->input('geo','');
        $show_geo = (int)$request->input('show_geo',0);

//        $group = array( 'campaign','customer','date','aff_pub');
        $group = array( 'campaign','customer');

        $getFiled=array(
            \DB::raw("CONCAT(c.name,'#',c.id) as campaign_name"),
            \DB::raw("CONCAT(a.name,'#',a.id) as advertiser_name"),
            \DB::raw("CONCAT(cu.name,'#',cu.id) as customer_name"),
            'c.package_name as package_name',
            \DB::raw('SUM(revenue) as revenue'),
            \DB::raw('SUM(payment) as payment')
        );

        if(!!$show_geo){
            array_push($headers,'Country');
            array_push($getFiled,'c.geo as geo');
        }

        if(!!$show_daily){
            array_push($group,'date');
            array_unshift($getFiled,\DB::raw("date_format(date,'%Y-%m-%d') as date"));
            array_unshift($headers,'Date');
        }

        if(!!$aff_pub){
            array_push($group,'aff_pub');
            array_push($getFiled,'aff_pub','aff_key',\DB::raw('count(*) as postback'),'table_a.callback as callback');
            array_push($headers,'Sub-affiliate','Sub-security','Postback','Callback');
            $temp_a=Postback::where('take','=',0)
                ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
                ->where('aff_pub', '!=', '')
                ->selectRaw('campaign as p_campaign,customer as p_customer,aff_pub as p_aff_pub,count(take) as callback,date as p_date');
            if(!!$show_daily){
                $temp_a = $temp_a->groupBy('p_campaign','p_customer','p_aff_pub','p_date');
            }else{
                $temp_a = $temp_a->groupBy('p_campaign','p_customer','p_aff_pub');
            }

            $query = Postback::query()
                ->leftJoin('_tb_campaign AS c','_tb_postback.campaign','=','c.id')
                ->leftJoin('_tb_advertiser AS a','c.advertiser','=','a.id')
                ->leftJoin('_tb_customer AS cu','_tb_postback.customer','=','cu.id')
                ->leftJoinSub($temp_a,'table_a',function(QBuilder $query) use ($aff_pub){
                    $q = $query->on('_tb_postback.campaign','=','table_a.p_campaign')
                        ->on('_tb_postback.customer','=','table_a.p_customer')
                        ->on('_tb_postback.aff_pub','=','table_a.p_aff_pub');
                    if(!!$aff_pub){
                        $q =$query->on('_tb_postback.date','=','table_a.p_date');
                    }
                    return $q;
                })
                ->where('aff_pub', '!=', '');
        }else{
            array_push($getFiled,
                \DB::raw("CONCAT(CAST(SUM(_tb_summarry.postback)/SUM(_tb_summarry.tracking)*100 as DECIMAL(10,3)),'%') as cr"),
                \DB::raw('SUM(_tb_summarry.postback) as postback'),
                \DB::raw('SUM(_tb_summarry.callback) as callback'),
                \DB::raw('SUM(_tb_summarry.tracking) as tracking'));
            array_push($headers,'CR','Postback','Callback','Clicks');
            $query = Summarry::query()
                ->leftJoin('_tb_campaign AS c','_tb_summarry.campaign','=','c.id')
                ->leftJoin('_tb_advertiser AS a','c.advertiser','=','a.id')
                ->leftJoin('_tb_customer AS cu','_tb_summarry.customer','=','cu.id');
        }




        $query->when($advertiser, function ($query) use ($advertiser) {
            return $query->advertiser($advertiser);
        })
            ->when($offers, function ($query) use ($offers) {
                $query->whereHas('campaign', function ($sub_query) use ($offers) {
                    $sub_query->whereIn('id',$offers);
                });
            })
            ->when($package_name, function ($query) use ($package_name) {
                $query->whereHas('campaign', function ($sub_query) use ($package_name) {
                    $sub_query->where('package_name', 'like', '%' . $package_name . '%');
                });
            })
            ->when($geo, function ($query) use ($geo) {
                $query->whereHas('campaign', function ($sub_query) use ($geo) {
                    $sub_query->where('geo', 'like', '%' . $geo . '%');
                });
            })
            ->when($affiliate, function ($query) use ($affiliate) {
                $query->where('customer',$affiliate);
            })
            ->when($search_aff_pub, function ($query) use ($search_aff_pub) {
                $query->where('aff_pub','=',$search_aff_pub);
            })
            ->when($search_aff_key, function ($query) use ($search_aff_key) {
                $query->where('aff_key','like','%'.$search_aff_key.'%');
            })
            ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);
        $data = $query->groupBy($group)
            ->orderBy($filed,$sort_type)
            ->select($getFiled)->get()->toArray();
        $cellData = array_merge([$headers], $data);

        $fileName = 'Report_'.time();

        return Excel::create($fileName,function($excel) use ($cellData){
            $excel->sheet('sheet1', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->download('xlsx');
    }

    public function exportConversionReport(PostbackCreateRequest $request)
    {
        $headers=['Offer name','Advertiser','Date','Conversion Id','Aff click','OSID','Affiliate','Sub-affiliate','Margin','Status','Revenue','Payment'];
        $advertiser = $request->get('advertiser');
        $offers = $request->get('offers');
        $package_name = $request->get('package_name');
        $affiliate = $request->get('affiliate');
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];
        $filed = $request->input('filed','date');
        $sort_type = $request->input('sort_type','desc');

        $getFiled=array(
            \DB::raw("CONCAT(c.name,'#',c.id) as campaign_name"),
            \DB::raw("CONCAT(a.name,'#',a.id) as advertiser_name"),
            \DB::raw("date_format(date,'%Y-%m-%d') as date"),
            'funny',
            'affclick',
            \DB::raw("(CASE WHEN (idfa != '') THEN idfa WHEN (gaid != '') THEN gaid ELSE '---' END) as os_id"),
            \DB::raw("CONCAT(cu.name,'#',cu.id) as customer_name"),
            'aff_pub',
            \DB::raw("_tb_postback.margin as margin"),
            \DB::raw("case when take>0 then 'Pending' else 'Approve' end as take"),
            'revenue',
            'payment',
        );

        $query = Postback::query()
            ->leftJoin('_tb_campaign AS c','_tb_postback.campaign','=','c.id')
            ->leftJoin('_tb_advertiser AS a','c.advertiser','=','a.id')
            ->leftJoin('_tb_customer AS cu','_tb_postback.customer','=','cu.id')
            ->when($advertiser, function ($query) use ($advertiser) {
                return $query->advertiser($advertiser);
            })
            ->when($offers, function ($query) use ($offers) {
                $query->whereHas('campaign', function ($sub_query) use ($offers) {
                    $sub_query->whereIn('id',$offers);
                });
            })
            ->when($package_name, function ($query) use ($package_name) {
                $query->whereHas('campaign', function ($sub_query) use ($package_name) {
                    $sub_query->where('package_name', 'like', '%' . $package_name . '%');
                });
            })
            ->when($affiliate, function ($query) use ($affiliate) {
                $query->where('customer',$affiliate);
            })
            ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);

//        ->groupBy($group)
        $data = $query
            ->orderBy($filed,$sort_type)
            ->select($getFiled)->get()->toArray();

        $cellData = array_merge([$headers], $data);

        $fileName = 'Conversion_Report_'.time();

        return Excel::create($fileName,function($excel) use ($cellData){
            $excel->sheet('sheet1', function($sheet) use ($cellData){
                $sheet->rows($cellData);
            });
        })->download('xlsx');
    }
}
