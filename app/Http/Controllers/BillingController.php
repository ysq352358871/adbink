<?php


namespace Someline\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Someline\Models\Billing;
use Someline\Models\Summarry;
use Someline\Models\Advertiser;
use Someline\Models\Affiliate;

class BillingController extends BaseController
{

    public function generateBilling()
    {

        $start = new Carbon('first day of last month');

        $start_date = $start->toDateString();

        $end = new Carbon('last day of last month');

        $end_date = $end->toDateString();

        $getFiled=array(
            'date',
            'campaign',
            'customer',
            \DB::raw("date_format(date,'%Y-%m') as date"),
            \DB::raw('SUM(postback) as postback'),
            \DB::raw('SUM(callback) as callback'),
            \DB::raw('SUM(revenue) as revenue'),
            \DB::raw('SUM(payment) as payment')
        );

        $data = Summarry::whereBetween(\DB::raw("date_format(date,'%Y-%m-%d')"),[$start_date,$end_date])
            ->where('postback','>',0)
            ->groupBy('campaign','customer',\DB::raw("date_format(date,'%Y-%m')"))
            ->orderby('campaign')
            ->select($getFiled)->get()->toArray();
        $chunk_result = array_chunk($data, 1000);

        foreach($chunk_result as $new_data) {
            Billing::insertOrIgnore($new_data);
        }
    }

    public function show(Request $request,$id)
    {
        $advertiser = Advertiser::find($id);
        $month = $request->get('month');

        return view('app.billing.advertiser_show', [
            'advertiser'=>$advertiser,
            'month'=>$month
        ]);
    }

    public function showAff(Request $request,$id)
    {
        $affiliate = Affiliate::find($id);
        $month = $request->get('month');

        return view('app.billing.affiliate_show', [
            'affiliate'=>$affiliate,
            'month'=>$month
        ]);
    }

}