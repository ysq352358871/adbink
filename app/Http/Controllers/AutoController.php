<?php


namespace Someline\Http\Controllers;

use Carbon\Carbon;
use Someline\Models\Auto;
use Someline\Models\Campaign;
use Someline\Models\Relation;

class AutoController extends BaseController
{
    public function automaticallyApprove()
    {
        $now = Carbon::now()->toDateTimeString();
        $campaigns = Campaign::query()
            ->join('_tb_auto',function ($q){
                return $q->on('_tb_campaign.advertiser','=','_tb_auto.advertiser')
                    ->on('_tb_campaign.package_name','like',\DB::raw("CONCAT('%',_tb_auto.package_name,'%')"));
            })->join('_tb_customer',function ($q){
                return $q->on('_tb_customer.id','=','_tb_auto.customer');
            })->join('_tb_jump',function ($q){
                return $q->on('_tb_campaign.id','=','_tb_jump.campaign_id')
                    ->on('_tb_jump.jump_data','<=','_tb_auto.jump');
            })
            ->select([
                '_tb_campaign.id as campaign',
                '_tb_auto.customer as customer',
                \DB::raw('_tb_campaign.price*_tb_customer.discount/100 as price'),
                '_tb_auto.daily as daily',
                '_tb_campaign.live as live'
            ])
            ->where([
                ['_tb_campaign.live','=',1],
                ['_tb_campaign.pull','=',1],
                ['_tb_jump.take','=',1],
                ['_tb_jump.jump_data','>',0]
            ])->get()->toArray();
//            ->chunkById(1000, function ($campaigns) use($now){
//                $relation = new Relation();
//                $updateColumns = ['daily','live','created_at','updated_at'];
//                foreach ($campaigns as $key => $campaign) {
//                    $campaign->campaign = $campaign->id;
//                    unset($campaign->id);
//                    $campaign->created_at = $now;
//                }
//            });
        foreach ($campaigns as $key => $campaign) {
            $campaign['created_at'] = $now;
        }
        $updateColumns = ['daily','live','created_at','updated_at'];
        $relation = new Relation();
        $relation->batchInsertOrUpdate($campaigns,'',[],$updateColumns);
//        Relation::insertOrIgnore($campaigns);
    }
}