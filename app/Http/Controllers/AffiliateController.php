<?php


namespace Someline\Http\Controllers;

use Someline\Models\Affiliate;

class AffiliateController
{
    public function show($id)
    {
        $affiliate = Affiliate::find($id);
//        dd($affiliate->campaigns[0]->pivot->price);
//        $data = array(
//            '60000'=>array(
//                'price'=>1.00,
//                'daily'=>200,
//            ),
//        );
//        dd($data);
//        $affiliate->campaigns()->attach($data);
        return view('app.affiliates.show', [
            'affiliate_id' => $id,
            'affiliate'=>$affiliate
        ]);
    }

    public function getAffiliateEdit($id)
    {
        $affiliate = Affiliate::find($id);
        return view('app.affiliates.edit', [
            'affiliate_id' => $id,
            'affiliate'=>$affiliate
        ]);
    }

}