<?php


namespace Someline\Http\Controllers\Affiliate;


use Someline\Http\Controllers\BaseController;
use Someline\Models\Campaign;
use Someline\Models\Country;

class OfferController extends BaseController
{

    public function Index()
    {
        return view('console.offers.index');
    }

    public function show($id)
    {
        $offer = Campaign::find($id);

        $offer->geo = Country::whereIn('code',explode(',',$offer->geo))->pluck('country');

        return view('console.offers.show',[
            'offer'=>$offer
        ]);
    }
}