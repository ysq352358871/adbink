<?php

namespace Someline\Http\Controllers\affiliate\Auth;

use Illuminate\Http\Request;
use Someline\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Password;
use Illuminate\Support\Facades\Mail;
use Someline\Models\Affiliate;
use Someline\Mail\ForgotPasswordEmail;

class ForgotPasswordController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:affiliate');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('auth.affiliate.passwords.email');
    }


    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $email = $request->get('email');
        $q = Affiliate::where('email', $email);
        $has_email = $q->exists();
        $user = $q->first();
        if ($has_email) {
            $token = str_random_except(32);
           Mail::to($user->email)->send(new ForgotPasswordEmail($token,$user->email));
        } else {
            return back()->with('error', 'We can\'t find a user with that e-mail address');
        }
        $success = 'An email to reset your password has been sent to your registered email address.';
        return back()->with('success', $success);

    }
}
