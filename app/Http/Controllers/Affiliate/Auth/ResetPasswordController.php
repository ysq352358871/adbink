<?php


namespace Someline\Http\Controllers\Affiliate\Auth;

use Hash;
use Illuminate\Http\Request;
use Someline\Models\Affiliate;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController
{
    public function showResetForm(Request $request,$token=null)
    {
        $email = $request->get('email');

        return view('auth.affiliate.passwords.reset')->with(['token' => $token]);
    }

    public function reset(Request $request)
    {
        $request->validate([
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string','min:8', 'confirmed'],
        ]);

        $email = $request->get('email');
        $password = Hash::make($request->get('password'));

        $aff = Affiliate::where('email',$email)->first();

        $aff->password = $password;
        $aff->save();

        Auth::guard('affiliate')->login($aff);

        return redirect(route('affiliate.home'));
    }
}