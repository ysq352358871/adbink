<?php


namespace Someline\Http\Controllers\Affiliate\Auth;

use Illuminate\Support\Facades\Auth;
use Someline\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AffiliateLoginController extends BaseController
{
    use AuthenticatesUsers;


    protected $redirectTo = 'affiliate/';



    public function __construct()
    {
        $this->middleware('guest:affiliate')->except('logout');
    }


    public function showLoginForm()
    {
        return view('auth.affiliate.login');
    }

    public function postLogin(Request $request)
    {
        $data = $request->only('email', 'password');

        $result = Auth::guard('affiliate')->attempt($data, true);

        if ($result) {

            return redirect(route('affiliate.home'));

        } else {

            return redirect()->back()

                ->with('email', $request->get('email'))

                ->withErrors(['email' => 'Wrong user name or password']);

        }
    }

    public function logout()
    {
        Auth::guard('affiliate')->logout();

        return redirect(route('affiliate.login'));
    }

    protected function guard()
    {
        return Auth::guard('affiliate');
    }

    public function username()
    {
        return 'email';
    }

}