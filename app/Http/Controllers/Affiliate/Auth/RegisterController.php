<?php


namespace Someline\Http\Controllers\Affiliate\Auth;

use Hash;
use Illuminate\Http\Request;
use Someline\Models\Affiliate;
use Illuminate\Support\Facades\Auth;

class RegisterController
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string','min:8', 'confirmed'],
        ]);
        $email = $request->get('email');
        $has_email = Affiliate::where('email', $email)->exists();

        if ($has_email) {
            return back()->with('error', 'The email has already been taken.');
        }
        $data = $request->all();

        unset($data['password_confirmation']);

        $data['password'] = Hash::make($request->get('password'));

        $aff = Affiliate::create($data);

        Auth::guard('affiliate')->login($aff);

        return redirect(route('affiliate.home'));

    }
}