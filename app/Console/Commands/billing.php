<?php

namespace Someline\Console\Commands;

use Illuminate\Console\Command;
use Someline\Http\Controllers\BillingController;

class billing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:billing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate billing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bill = new BillingController();
        $bill->generateBilling();
    }
}
