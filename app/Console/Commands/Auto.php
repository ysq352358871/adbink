<?php

namespace Someline\Console\Commands;

use Illuminate\Console\Command;
use Someline\Http\Controllers\AutoController;

class Auto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically approve offers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bill = new AutoController();
        $bill->automaticallyApprove();
    }
}
