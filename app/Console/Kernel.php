<?php

namespace Someline\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Someline\Console\Commands\billing;
use Someline\Console\Commands\Invoice;
use Someline\Console\Commands\Auto;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        billing::class,
        Invoice::class,
        Auto::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command(billing::class)
             ->monthlyOn(1, '01:00');

        $schedule->command(Invoice::class)
            ->monthlyOn(1, '01:00');

        $schedule->command(Auto::class)
            ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
