<?php

namespace Someline\Presenters;

use Someline\Transformers\AffiliateTransformer;

/**
 * Class AffiliatePresenter
 *
 * @package namespace Someline\Presenters;
 */
class AffiliatePresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AffiliateTransformer();
    }
}
