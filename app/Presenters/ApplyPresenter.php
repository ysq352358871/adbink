<?php

namespace Someline\Presenters;

use Someline\Transformers\ApplyTransformer;

/**
 * Class ApplyPresenter
 *
 * @package namespace Someline\Presenters;
 */
class ApplyPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ApplyTransformer();
    }
}
