<?php

namespace Someline\Presenters;

use Someline\Transformers\InvoiceTransformer;

/**
 * Class InvoicePresenter
 *
 * @package namespace Someline\Presenters;
 */
class InvoicePresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new InvoiceTransformer();
    }
}
