<?php

namespace Someline\Presenters;

use Someline\Transformers\OpmarkTransformer;

/**
 * Class OpmarkPresenter
 *
 * @package namespace Someline\Presenters;
 */
class OpmarkPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OpmarkTransformer();
    }
}
