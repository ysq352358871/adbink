<?php

namespace Someline\Presenters;

use Someline\Transformers\OutlayTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OutlayPresenter.
 *
 * @package namespace Someline\Presenters;
 */
class OutlayPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OutlayTransformer();
    }
}
