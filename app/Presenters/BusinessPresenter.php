<?php

namespace Someline\Presenters;

use Someline\Transformers\BusinessTransformer;

/**
 * Class BusinessPresenter
 *
 * @package namespace Someline\Presenters;
 */
class BusinessPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BusinessTransformer();
    }
}
