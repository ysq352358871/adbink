<?php

namespace Someline\Presenters;

use Someline\Transformers\ScheduleTransformer;

/**
 * Class SchedulePresenter
 *
 * @package namespace Someline\Presenters;
 */
class SchedulePresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ScheduleTransformer();
    }
}
