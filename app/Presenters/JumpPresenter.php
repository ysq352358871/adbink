<?php

namespace Someline\Presenters;

use Someline\Transformers\JumpTransformer;

/**
 * Class JumpPresenter
 *
 * @package namespace Someline\Presenters;
 */
class JumpPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new JumpTransformer();
    }
}
