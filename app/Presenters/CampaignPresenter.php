<?php

namespace Someline\Presenters;

use Someline\Transformers\CampaignTransformer;

/**
 * Class CampaignPresenter
 *
 * @package namespace Someline\Presenters;
 */
class CampaignPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CampaignTransformer();
    }
}
