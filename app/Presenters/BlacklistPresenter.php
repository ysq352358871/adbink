<?php

namespace Someline\Presenters;

use Someline\Transformers\BlacklistTransformer;

/**
 * Class BlacklistPresenter
 *
 * @package namespace Someline\Presenters;
 */
class BlacklistPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BlacklistTransformer();
    }
}
