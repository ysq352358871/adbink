<?php

namespace Someline\Presenters;

use Someline\Transformers\PostbackTransformer;

/**
 * Class PostbackPresenter
 *
 * @package namespace Someline\Presenters;
 */
class PostbackPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PostbackTransformer();
    }
}
