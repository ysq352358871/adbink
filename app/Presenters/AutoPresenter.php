<?php

namespace Someline\Presenters;

use Someline\Transformers\AutoTransformer;

/**
 * Class AutoPresenter
 *
 * @package namespace Someline\Presenters;
 */
class AutoPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AutoTransformer();
    }
}
