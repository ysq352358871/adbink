<?php

namespace Someline\Presenters;

use Someline\Transformers\RelationTransformer;

/**
 * Class RelationPresenter
 *
 * @package namespace Someline\Presenters;
 */
class RelationPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RelationTransformer();
    }
}
