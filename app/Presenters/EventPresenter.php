<?php

namespace Someline\Presenters;

use Someline\Transformers\EventTransformer;

/**
 * Class EventPresenter
 *
 * @package namespace Someline\Presenters;
 */
class EventPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EventTransformer();
    }
}
