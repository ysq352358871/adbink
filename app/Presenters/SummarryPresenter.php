<?php

namespace Someline\Presenters;

use Someline\Transformers\SummarryTransformer;

/**
 * Class SummarryPresenter
 *
 * @package namespace Someline\Presenters;
 */
class SummarryPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SummarryTransformer();
    }
}
