<?php

namespace Someline\Presenters;

use Someline\Transformers\BillingTransformer;

/**
 * Class BillingPresenter
 *
 * @package namespace Someline\Presenters;
 */
class BillingPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BillingTransformer();
    }
}
