<?php

namespace Someline\Presenters;

use Someline\Transformers\AdvertiserTransformer;

/**
 * Class AdvertiserPresenter
 *
 * @package namespace Someline\Presenters;
 */
class AdvertiserPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AdvertiserTransformer();
    }
}
