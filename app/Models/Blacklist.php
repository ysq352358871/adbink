<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Blacklist extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

//    protected $fillable = ['campaign','customer','aff_pub'];
    protected $guarded = ['id'];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_blacklist';

}
