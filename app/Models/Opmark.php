<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Someline\Models\Foundation\User;

class Opmark extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

//    protected $fillable = [];
    protected $guarded = ['id'];
    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_opmarks';

    const TYPE = 'Someline\Models\Opmark';

    public function schedules()
    {
        return $this->morphMany(Schedule::class, 'scheduleable');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
