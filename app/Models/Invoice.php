<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Invoice extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

    protected $fillable = [
        'month','campaign','customer','amount','pay_method','pay_amount','link',
        'email_send_time','mark','paid'
    ];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_invoice';

    public function advertiser()
    {
        return $this->belongsTo(Advertiser::class, 'advertiser');
    }

    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'customer');
    }

}
