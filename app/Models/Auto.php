<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Auto extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

    protected $fillable = [
        'advertiser','customer','package_name','jump','group_key',
        'group_name',
        'daily',
        'created_at',
        'updated_at'
    ];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_auto';

    /**
     * 用来向表中插入数据的字段
     *
     * @var array
     */
    protected $tableColumns = [
        'advertiser',
        'customer',
        'package_name',
        'jump',
        'group_key',
        'group_name',
        'daily',
        'created_at',
        'updated_at'
    ];



    public function billing()
    {
        return $this->belongsTo(Advertiser::class, 'advertiser');
    }

    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'customer');
    }

}
