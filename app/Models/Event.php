<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Event extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

    protected $fillable = ['campaign_id','event_token','status','active'];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_event';

}
