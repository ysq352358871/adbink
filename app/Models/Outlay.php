<?php

namespace Someline\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Outlay.
 *
 * @package namespace Someline\Models;
 */
class Outlay extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
//    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected  $table='_tb_outlays';
}
