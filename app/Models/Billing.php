<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Billing extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

    protected $fillable = [
        'date','campaign','customer','postback','callback','revenue','payment',
        'deduct','pub_reduce','status'
    ];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];
    protected  $table='_tb_billing';


    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign');
    }

    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'customer');
    }

}
