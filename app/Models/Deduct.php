<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Deduct.
 *
 * @package namespace Someline\Models;
 */
class Deduct extends BaseModel implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date','campaign','customer','file_link'
    ];
    protected  $table='_tb_deduct';

    protected $tableColumns = [
        'date',
        'campaign',
        'customer',
        'file_link',
        'created_at',
        'updated_at'
    ];

}
