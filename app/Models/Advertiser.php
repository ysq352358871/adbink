<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Someline\Models\Foundation\User;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Advertiser extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_advertiser';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class,'id');
    }

    public function summarries()
    {
        return $this->hasManyThrough(
            Summarry::class,
            Campaign::class,
            'advertiser',
            'campaign',
            'id',
            'id'
        );
    }

    public function total()
    {
        return $this->summarries();
    }

    public function scopeWhereContentContain($query, $search)
    {
        return $query->where('id', 'like', '%' . $search . '%')
            ->orWhere('name', 'like', '%' . $search . '%')
            ->orWhere('email', 'like', '%' . $search . '%')
            ->orWhere('company_name', 'like', '%' . $search . '%')
            ->orWhere('im', 'like', '%' . $search . '%')
            ->orWhere('contact_name', 'like', '%' . $search . '%');
    }

}
