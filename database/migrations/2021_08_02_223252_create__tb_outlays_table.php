<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateOutlaysTable.
 */
class CreateTbOutlaysTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_tb_outlays', function(Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('month',100)->index();
            $table->string('type',100)->default('');
            $table->decimal('amount',10,2)->default(0)->nullable();
            $table->string('currency',40)->default('usd');
            $table->string('mark',255)->default('')->nullable();

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_tb_outlays');
	}
}
