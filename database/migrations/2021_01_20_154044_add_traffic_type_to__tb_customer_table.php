<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrafficTypeToTbCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_tb_customer', function (Blueprint $table) {
            $table->string('traffic_type',255)->after('exclude')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_tb_customer', function (Blueprint $table) {
            $table->dropColumn('traffic_type');
        });
    }
}
