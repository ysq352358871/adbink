<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupNameToTbAutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_tb_auto', function (Blueprint $table) {
            $table->string('group_key',40)->after('jump')->default('')->index();
            $table->string('group_name',255)->after('group_key')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_tb_auto', function (Blueprint $table) {
            $table->dropColumn('group_key');
            $table->dropColumn('group_name');
        });
    }
}
