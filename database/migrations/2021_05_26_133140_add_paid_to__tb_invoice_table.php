<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaidToTbInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_tb_invoice', function (Blueprint $table) {
            $table->string('pay_method',255)->after('amount')->nullable();
            $table->decimal('pay_amount',10,3)->after('pay_method')->default(0)->nullable();
            $table->tinyInteger('paid')->after('invoice_date')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_tb_invoice', function (Blueprint $table) {
            //
        });
    }
}
