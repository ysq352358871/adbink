<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPackageNameColumnInTbCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_tb_campaign', function (Blueprint $table) {
            $table->string('package_name',255)->index()->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_tb_campaign', function (Blueprint $table) {
            $table->dropColumn('package_name');
        });
    }
}
