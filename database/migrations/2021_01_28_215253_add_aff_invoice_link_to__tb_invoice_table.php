<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffInvoiceLinkToTbInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_tb_invoice', function (Blueprint $table) {
            $table->string('aff_invoice_link',255)->after('link')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_tb_invoice', function (Blueprint $table) {
            $table->dropColumn('aff_invoice_link');
        });
    }
}
