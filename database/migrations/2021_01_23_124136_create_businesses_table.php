<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('_tb_businesses', function(Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('user_id')->index();

            // Adding more table related fields here...
            $table->tinyInteger('top')->default(0)->index();
            $table->string('client_type',60)->default('')->nullable();
            $table->string('name',255)->default('');
            $table->text('mark')->default('');
            $table->string('contact',255)->default('');
            $table->tinyInteger('status')->default(0);

            $table->unsignedInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->ipAddress('created_ip')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->ipAddress('updated_ip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('_tb_businesses');
	}

}
