<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbAutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_tb_auto', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('advertiser');
            $table->integer('customer');
            $table->string('package_name')->default('')->nullable();
            $table->integer('jump')->default(5);
            $table->timestamps();
            $table->unique(['advertiser', 'customer']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tb_auto');
    }
}
