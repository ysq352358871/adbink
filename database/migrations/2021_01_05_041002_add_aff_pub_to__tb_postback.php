<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffPubToTbPostback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_tb_postback', function (Blueprint $table) {
            $table->unsignedInteger('aff_pub')->after('customer')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_tb_postback', function (Blueprint $table) {
            $table->dropColumn('aff_pub');
        });
    }
}
