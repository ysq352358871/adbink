<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbDeductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_tb_deduct', function (Blueprint $table) {
            $table->primary(['date', 'campaign','customer']);
            $table->string('date',100)->index()->nullable();
            $table->unsignedInteger('campaign')->index()->default(0)->nullable();
            $table->unsignedInteger('customer')->index()->default(0)->nullable();
            $table->string('file_link',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tb_deduct');
    }
}
