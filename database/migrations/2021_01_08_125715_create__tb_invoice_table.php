<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_tb_invoice', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('month',100)->index()->nullable();
            $table->unsignedInteger('advertiser')->index()->default(0)->nullable();
            $table->unsignedInteger('customer')->index()->default(0)->nullable();
            $table->decimal('amount',10,3)->default(0)->nullable();
            $table->string('link',255)->nullable();
            $table->timestamp('email_send_time')->nullable();
            $table->string('mark',255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tb_invoice');
    }
}
