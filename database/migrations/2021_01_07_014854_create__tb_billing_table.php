<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_tb_billing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date',100)->index()->nullable();
            $table->unsignedInteger('campaign')->index()->default(0)->nullable();
            $table->unsignedInteger('customer')->index()->default(0)->nullable();
            $table->unsignedInteger('postback')->default(0)->nullable();
            $table->unsignedInteger('callback')->default(0)->nullable();
            $table->decimal('revenue',10,3)->default(0)->nullable();
            $table->decimal('payment',10,3)->default(0)->nullable();
            $table->decimal('deduct',10,3)->default(0)->nullable();
            $table->decimal('pub_reduce',10,3)->default(0)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tb_billing');
    }
}
