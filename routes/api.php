<?php

use Illuminate\Http\Request;
use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// v1
$api->version('v1', [
    'namespace' => 'Someline\Api\Controllers',
    'middleware' => ['api']
], function (Router $api) {

    $api->group(['middleware' => ['auth:api']], function (Router $api) {

        // Rate: 100 requests per 5 minutes
        $api->group(['middleware' => ['api.throttle'], 'limit' => 100, 'expires' => 5], function (Router $api) {

            // /users
            $api->group(['prefix' => 'users'], function (Router $api) {
                $api->get('/', 'UsersController@index');
                $api->get('/all', 'UsersController@all');
                $api->post('/', 'UsersController@store');
                $api->get('/me', 'UsersController@me');
                $api->get('/{id}', 'UsersController@show');
                $api->put('/{id}', 'UsersController@update');
                $api->delete('/{id}', 'UsersController@destroy');
            });

            // /campaigns
            $api->group(['prefix' => 'campaigns'], function (Router $api) {
                $api->get('/', 'CampaignsController@index');
                $api->post('/', 'CampaignsController@store');
                $api->get('/top', 'CampaignsController@top');
                $api->get('/s', 'CampaignsController@search');
                $api->put('/setStatus', 'CampaignsController@updateStatus');
                $api->get('/{id}', 'CampaignsController@show');
                $api->put('/{id}', 'CampaignsController@update');
                $api->get('/{id}/events', 'CampaignsController@getEvents');
                $api->get('/{id}/affiliate','CampaignsController@getRelationAffiliates');
            });

            //Affiliates
            $api->group(['prefix' => 'affiliates'], function (Router $api) {
                $api->get('/', 'AffiliatesController@index');
                $api->post('/', 'AffiliatesController@store');
                $api->get('/all', 'AffiliatesController@all');
                $api->get('/top', 'AffiliatesController@top');
                $api->get('/{id}/topOffers', 'AffiliatesController@topOffers');
                $api->get('/{id}', 'AffiliatesController@show');
                $api->put('/{id}', 'AffiliatesController@update');
                $api->get('/{id}/offers', 'AffiliatesController@getRelationOffers');
                $api->put('/{id}/apikey', 'AffiliatesController@updateKey');
                $api->post('/{id}/email', 'AffiliatesController@sendBillingEmail');
                $api->delete('/{id}/offers/delete', 'AffiliatesController@destroyAll');
            });

            //Advertisers
            $api->group(['prefix' => 'advertisers'], function (Router $api) {
                $api->get('/', 'AdvertisersController@index');
                $api->post('/', 'AdvertisersController@store');
                $api->get('/all', 'AdvertisersController@all');
                $api->get('/weekly', 'AdvertisersController@weekly');
                $api->get('/top', 'AdvertisersController@top');
                $api->get('/{id}', 'AdvertisersController@show');
                $api->put('/{id}', 'AdvertisersController@update');
                $api->post('/{id}/email', 'AdvertisersController@sendInvoiceEmail');
            });

            //Relations
            $api->group(['prefix' => 'relations'], function (Router $api) {
                $api->post('/apply', 'RelationsController@store');
                $api->put('/status', 'RelationsController@changeStatus');
                $api->put('/{campaign}/{customer}', 'RelationsController@update');
                $api->put('/update_pubx', 'RelationsController@updatePubx');
            });

            //Events
            $api->group(['prefix' => 'events'], function (Router $api) {
                $api->post('/', 'EventsController@store');
                $api->put('/{id}/updateOthers', 'EventsController@updateOthersEvent');
                $api->put('/{id}', 'EventsController@update');
                $api->delete('/{id}', 'EventsController@destroy');
            });

            //report
            $api->group(['prefix' => 'report'], function (Router $api) {
                $api->get('/general', 'SummarriesController@getGeneralReport');
                $api->get('/conversion', 'PostbacksController@getConversionReport');
                $api->get('/daily', 'SummarriesController@getDailyData');
            });

            //blacklist
            $api->group(['prefix' => 'blacks'], function (Router $api) {
                $api->post('/add', 'BlacklistsController@store');
            });

            //applied
            $api->group(['prefix' => 'applied'], function (Router $api) {
                $api->get('/offers', 'AppliesController@index');
                $api->put('/offers', 'AppliesController@applyOffer');
            });

            //billing
            $api->group(['prefix' => 'billing'], function (Router $api) {
                $api->get('/', 'BillingsController@index');
                $api->get('/{id}', 'BillingsController@show');
                $api->get('/{id}/affiliate', 'BillingsController@showAffiliate');
                $api->put('/dedcut', 'BillingsController@update');
            });

            //invoice
            $api->group(['prefix' => 'invoice'], function (Router $api) {
                $api->get('/finance','InvoicesController@getOutlays');
                $api->get('/{type}', 'InvoicesController@index');
                $api->put('/{id}', 'InvoicesController@update');
                $api->post('/adv/{id}','UploadController@generateAdvInvoice');
                $api->post('/aff','UploadController@generateAffBilling');
            });

            // auto
            $api->group(['prefix' => 'auto'], function (Router $api) {
                $api->get('/', 'AutosController@index');
                $api->post('/', 'AutosController@store');
                $api->delete('/del', 'AutosController@destroy');
            });

            //Business
            $api->group(['prefix' => 'business'], function (Router $api) {
                $api->get('/', 'BusinessesController@index');
                $api->post('/', 'BusinessesController@store');
                $api->put('/{id}', 'BusinessesController@update');
                $api->post('/{id}/schedule', 'BusinessesController@createSchedule');
                $api->get('/{id}/schedule', 'BusinessesController@getSchedule');
            });

            // Opmarks
            $api->group(['prefix' => 'opmark'], function (Router $api) {
                $api->get('/', 'OpmarksController@index');
                $api->post('/', 'OpmarksController@store');
                $api->put('/{id}', 'OpmarksController@update');
                $api->post('/{id}/schedule', 'OpmarksController@createSchedule');
                $api->get('/{id}/schedule', 'OpmarksController@getSchedule');
            });

            //outlays
            $api->group(['prefix' => 'outlay'], function (Router $api) {
                $api->get('/', 'OutlaysController@index');
                $api->post('/', 'OutlaysController@store');
                $api->put('/{id}', 'OutlaysController@update');
            });

            //upload
            $api->group(['prefix' => 'u'], function (Router $api) {
                $api->post('/invoice/{id}','UploadController@uploadAffInvoice');
                $api->post('/import/billing','UploadController@importBilling');
                $api->post('/deduct','UploadController@uploadDeduct');
            });

        });

    });

    //apply
    $api->post('/apply', 'AppliesController@apply');

    //offer

    $api->get('/check','CampaignsController@canGetTrackingLink');

    // /country
    $api->group(['prefix' => 'countries'], function (Router $api) {
        $api->get('/', 'CountriesController@index');
        $api->get('/all', 'CountriesController@all');
    });

    // customer
    $api->group(['prefix' => 'customers'], function (Router $api) {
        $api->get('/offers', 'CampaignsController@getOffers');
        $api->get('/{id}/offers/search', 'AffiliatesController@getRelationOffers');
        $api->get('/{id}/offers', 'AffiliatesController@getCustomerOffers');
        $api->get('/{id}/offer/top', 'AffiliatesController@getCustomerTops');
        $api->get('/{id}/graph', 'AffiliatesController@getCustomerGraph');
        $api->get('/{id}/report', 'AffiliatesController@getCustomerReport');
    });

});