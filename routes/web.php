<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth Routes
Auth::routes();

// Basic Routes
//Route::get('/home', 'HomeController@index');

// Protected Routes
Route::group(['middleware' => 'auth'], function () {

//    Route::get('/', function () {
//        return redirect('users');
//    });

    Route::view('/', 'app.dashboard.index')->name('dashboard');

    //offers
    Route::group(['prefix' => 'offers'], function () {
        Route::view('/', 'app.offers.index')->name('offers');
        Route::view('/api', 'app.offers.api_index')->name('offers.api');
        Route::view('/top', 'app.offers.top')->name('offers.top');
        Route::get('/create', 'CampaignController@create')->name('offers.create');
        Route::view('/applied', 'app.offers.applied')->name('offers.applied');
        Route::get('/{id}', 'CampaignController@show')->name('offers.show');
        Route::get('/{id}/edit', 'CampaignController@getCampaignEdit')->name('offers.edit');
    });

    //Affiliates
    Route::group(['prefix' => 'affiliates'], function () {
        Route::view('/', 'app.affiliates.index')->name('affiliates');
        Route::view('/create', 'app.affiliates.create')->name('affiliates.create');
        Route::get('/{id}', 'AffiliateController@show')->name('affiliates.show');
        Route::get('/{id}/edit', 'AffiliateController@getAffiliateEdit')->name('affiliates.edit');
    });

    //Advertisers
    Route::group(['prefix' => 'advertisers'], function () {
        Route::view('/', 'app.advertisers.index')->name('advertisers');
        Route::view('/create', 'app.advertisers.create')->name('advertisers.create');
        Route::get('/{id}', 'AdvertiserController@show')->name('advertisers.show');
        Route::get('/{id}/edit', 'AdvertiserController@getAdvertiserEdit')->name('advertisers.edit');
    });

    Route::group(['prefix' => 'automation'], function () {
        Route::view('/', 'app.automation.index')->name('automation');
    });

    //Report
    Route::group(['prefix' => 'report'], function () {
        Route::view('/general', 'app.report.general_report')->name('report.general');
        Route::view('/conversion', 'app.report.conversion_report')->name('report.conversion');
        Route::get('/export', 'HomeController@exportReport')->name('report.export');
        Route::get('/exportConversion', 'HomeController@exportConversionReport')->name('report.export.conversion');
    });

    //Billing
    Route::group(['prefix' => 'billing'], function () {
        Route::view('/advertiser', 'app.billing.advertiser')->name('billing.advertiser');
        Route::get('/advertiser/{id}', 'BillingController@show')->name('billing.advertiser.show');
        Route::view('/affiliate', 'app.billing.affiliate')->name('billing.affiliate');
        Route::get('/affiliate/{id}', 'BillingController@showAff')->name('billing.affiliate.show');
    });

    //Invoice
    Route::group(['prefix' => 'invoice'], function () {
        Route::view('/advertiser', 'app.invoice.advertiser')->name('invoice.advertiser');
        Route::view('/affiliate', 'app.invoice.affiliate')->name('invoice.affiliate');
    });

    Route::view('/import-billing', 'app.import_billing')->name('import.billing');

    Route::view('/outlay', 'app.outlay.index')->name('outlay.index');
    Route::view('/finance', 'app.finance.index')->name('finance.index');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('/example', 'ExampleController@getIndexExample');
    Route::get('blank-example', 'ExampleController@getBlankExample');
    Route::get('desktop-example', 'ExampleController@getDesktopExample');

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UserController@all')->name('users');
        Route::view('/create', 'app.users.create')->name('users.create');
        Route::get('/{id}', 'UserController@show')->name('users.profile');
    });

    Route::get('generate_billing', 'BillingController@generateBilling');
    Route::get('generate_invoice', 'InvoiceController@generateInvoice');
    Route::get('total', 'TestController@getTotal');
});

// test router
Route::get('send', 'UserController@send');
Route::get('auto', 'AutoController@automaticallyApprove');
Route::get('billing', 'BillingController@generateBilling');
Route::get('invoice', 'InvoiceController@generateInvoice');
Route::get('pdf', 'TestController@testPdf');
Route::get('aff/pdf', 'TestController@testAffPdf');
Route::get('update_invoice', 'TestController@testUpdateInvoice');
Route::get('update_aff_invoice', 'TestController@testUpdateAffInvoice');
Route::get('import_billing', 'TestController@importBilling');
Route::get('export_report', 'TestController@exportReport');
Route::get('test', 'TestController@test');


// document /help/apidoc/v1.1
Route::group(['prefix' => 'help'], function () {
    Route::view('/apidoc', 'app.help.api')->name('help.api');
});


// Mobile Routes
Route::group(['prefix' => 'm', 'namespace' => 'Mobile'], function () {

    // Mobile App
    Route::get('/', 'MobileController@getIndex');

    Route::get('/app', 'MobileController@getApp');

    // Protected Routes
    Route::group(['middleware' => 'auth'], function () {

        Route::get('users', 'UserController@getUserList');

        Route::get('users/{id}', 'UserController@getUserDetail');

    });

});

// Console Routes
//Route::group(['prefix' => 'console', 'middleware' => 'auth', 'namespace' => 'Console'], function () {
//
//    Route::get('/', 'ConsoleController@getConsoleHome');
//    Route::get('oauth', 'ConsoleController@getOauth');
//    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
//
//    Route::group(['prefix' => 'users'], function () {
//        Route::get('/', 'UserController@getUserList');
//    });
//
//});
//新增的affiliate 后台
Route::group(['prefix' => 'affiliate','namespace'=>'Affiliate'], function(){

    Route::get('login', 'Auth\AffiliateLoginController@showLoginForm')->name('affiliate.login');

    Route::post('login', 'Auth\AffiliateLoginController@postLogin');

    Route::post('logout', 'Auth\AffiliateLoginController@logout')->name('affiliate.logout');

    Route::post('register', 'Auth\RegisterController@register')->name('affiliate.register');

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('Affiliate.password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('Affiliate.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('Affiliate.password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('Affiliate.password.update');

});
Route::group(['prefix' => 'affiliate','middleware' => 'auth.affiliate:affiliate','namespace'=>'Affiliate'], function(){

    Route::get('/', 'HomeController@Index')->name('affiliate.home');
    Route::get('/offers', 'OfferController@Index')->name('affiliate.offers');
    Route::view('/offers/mine', 'console.offers.mine')->name('affiliate.offers.mine');
    Route::get('/offers/{id}','OfferController@show')->name('affiliate.offers.show');
    Route::view('/report','console.report.index')->name('affiliate.report');

});



// Image Routes
// @WARNING: The 'image' prefix is reserved for SomelineImageService
Route::group(['prefix' => 'image'], function () {

    Route::group(['middleware' => 'auth'], function () {
        Route::post('/', 'ImageController@postImage');
    });

    Route::get('{type}/{name}', 'ImageController@showTypeImage');
    Route::get('/{name}', 'ImageController@showOriginalImage');

});

// Locale Routes
// @WARNING: The 'locales' prefix is reserved for SomelineLocaleController
Route::group(['prefix' => 'locales'], function () {

    Route::get('/{locale}.js', '\Someline\Support\Controllers\LocaleController@getLocaleJs');

    Route::get('/switch/{locale}', '\Someline\Support\Controllers\LocaleController@getSwitchLocale');

});


