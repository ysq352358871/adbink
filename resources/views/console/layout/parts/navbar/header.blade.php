<!-- navbar header -->
<div class="navbar-header bg-black">
    <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
        <i class="glyphicon glyphicon-cog"></i>
    </button>
    <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
        <i class="glyphicon glyphicon-align-justify"></i>
    </button>
    <!-- brand -->
    <a href="{{route('affiliate.home')}}" class="navbar-brand text-lt" style="display: block">
{{--        <img src="https://www.someline.com/images/logo/someline-icon-64.png" alt=".">--}}
{{--        <span class="hidden-folded m-l-xs">--}}
{{--            <img src="https://www.someline.com/images/logo/someline-logo@2x.png" style="max-height: 18px" alt=".">--}}
{{--        </span>--}}
        Appone
    </a>
    <!-- / brand -->
</div>
<!-- / navbar header -->