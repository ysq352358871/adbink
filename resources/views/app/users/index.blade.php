@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3">Users</h1>
        <a href="{{ route('users.create') }}"
           class="btn btn-sm btn-primary"
           target="_blank">Create User</a>
    </div>
    <div class="wrapper-md">
        <sl-user-list :users="{{ $users }}"></sl-user-list>
    </div>
@endsection


