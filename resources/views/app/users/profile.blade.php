@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3 pull-left">
            {{ $user->name }}
        </h1>
        <ul class="breadcrumb pull-right" style="margin-bottom: 0">
            <li><a href="{{ route('users') }}">Users</a></li>
            <li><a href="/users/{{ $user->id }}">{{ $user->name }}</a></li>
            <li style="color: #3da2df">{{ $user->name }}</li>
        </ul>
    </div>
    <div class="wrapper-md">
        <user-profile :user="{{ $user }}"></user-profile>
    </div>
@endsection




