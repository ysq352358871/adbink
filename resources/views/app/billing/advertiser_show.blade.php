@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3 pull-left">
            {{ $advertiser->name }}#{{ $advertiser->id }}/{{ $month }}
        </h1>
        <ul class="breadcrumb pull-right" style="margin-bottom: 0">
            <li><a href="{{ route('billing.advertiser') }}">Advertiser's billing</a></li>
            <li style="color: #3da2df">{{ $advertiser->name }}#{{ $advertiser->id }}</li>
        </ul>
    </div>
    <div class="wrapper-md">
        <advertiser-show :id="{{ $advertiser->id }}" month="{{ $month }}"></advertiser-show>
    </div>
@endsection



