@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3">Affiliate's billing</h1>
    </div>
    <div class="wrapper-md">
        <billing-affiliate-list></billing-affiliate-list>
    </div>
@endsection
