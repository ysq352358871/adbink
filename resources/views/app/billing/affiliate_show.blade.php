@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3 pull-left">
            {{ $affiliate->name }}#{{ $affiliate->id }}/{{ $month }}
        </h1>
        <ul class="breadcrumb pull-right" style="margin-bottom: 0">
            <li><a href="{{ route('billing.advertiser') }}">Affiliate's billing</a></li>
            <li style="color: #3da2df">{{ $affiliate->name }}#{{ $affiliate->id }}</li>
        </ul>
    </div>
    <div class="wrapper-md">
        <billing-affiliate-show :id="{{ $affiliate->id }}" month="{{ $month }}"></billing-affiliate-show>
    </div>
@endsection



