@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3 pull-left">
            {{ $affiliate->name }}
        </h1>
        <ul class="breadcrumb pull-right" style="margin-bottom: 0">
            <li><a href="{{ route('affiliates') }}">Affiliates</a></li>
            <li style="color: #3da2df">{{ $affiliate->name }}</li>
        </ul>
    </div>
    <div class="wrapper-md">
        <affiliate-edit :id="{{ $affiliate_id }}"></affiliate-edit>
    </div>
@endsection



