@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3">Affiliates</h1>
        <a href="{{ route('affiliates.create') }}"
           class="btn btn-sm btn-primary"
           target="_blank">Create Affiliate</a>
    </div>
    <div class="wrapper-md">
        <affiliate-list></affiliate-list>
    </div>
@endsection

