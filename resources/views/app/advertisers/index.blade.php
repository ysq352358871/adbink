@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3">Advertisers</h1>
        <a href="{{ route('advertisers.create') }}"
           class="btn btn-sm btn-primary"
           target="_blank">Create advertise</a>
    </div>
    <div class="wrapper-md">
        <advertisers-list></advertisers-list>
    </div>
@endsection
