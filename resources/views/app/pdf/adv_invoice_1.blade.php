<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            /*padding: 30px;*/
            /*border: 1px solid #eee;*/
            /*box-shadow: 0 0 10px rgba(0, 0, 0, .15);*/
            font-size: 14px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:last-child {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            width: 33.333%;
        }

        /*.invoice-box table tr.details td {*/
        /*    padding-bottom: 20px;*/
        /*}*/

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        /*.invoice-box table tr.item.last td {*/
        /*    border-bottom: none;*/
        /*}*/

        .invoice-box table tr.total td:last-child {
            font-weight: bold;
        }

        /*@media only screen and (max-width: 600px) {*/
        /*    .invoice-box table tr.top table td {*/
        /*        width: 100%;*/
        /*        display: block;*/
        /*        text-align: center;*/
        /*    }*/

        /*    .invoice-box table tr.information table td {*/
        /*        width: 100%;*/
        /*        display: block;*/
        /*        text-align: center;*/
        /*    }*/
        /*}*/

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
        .f-14{
            font-size: 14px
        }
        .f-16{
            font-size: 16px
        }
        .t-l{
            text-align: left!important;
        }
        .t-r{
            text-align: right!important;
        }
        .t-c{
            text-align: center!important;
        }
        .b-b{
            border-top: 1px solid #eee;
        }
        .b-t{
            border-top: 1px solid #eee;
        }
        p{
            margin: 0;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title" style="width: 45%">

                            <img src="data:image/jpeg;base64,{{ base64_encode(@file_get_contents(url('/assets/img/pdf/adbink.jpg'))) }}" style="width:100%; max-width:300px;">
                        </td>

                        <td class="f-16" style="width: 55%">
                            <b>Invoice NO #:</b> {{ $invoice["invoice_no"] }}<br>
                            <b>Currency:</b> USD<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            Invoice Date: {{ \Carbon\Carbon::parse($invoice_date)->format('Y/m/d') }}<br>
                            Start Date: {{ \Carbon\Carbon::parse($invoice['month'])->firstOfMonth()->format('Y/m/d') }}
                        </td>


                        <td class="t-l">
                            Due Date: {{ \Carbon\Carbon::parse($invoice_date)->addMonth(1)->format('Y/m/d') }}<br>
                            End Date: {{ \Carbon\Carbon::parse($invoice['month'])->lastOfMonth()->format('Y/m/d') }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2" class="b-t">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 50%">
                            <p style="margin-bottom: 16px"><b>From:</b></p>
                            ID: 2667776<br>
                            Name: Adbink Limited<br>
                            Address: RM18,27F,Ho King Comm CTR ,2-16 Fayuen  ST,Mongkok Kowloon HK<br>
                        </td>
                        <td class="t-l">
                            <p style="margin-bottom: 16px"><b>Billing To:</b></p>
                            Name: {{ $adv["company_name"] }}<br>
                            Address: {{ $adv["address"] }}<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2" style="padding-bottom: 20px">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr class="heading">
                        <td>
                            Item & Description
                        </td>
                        <td class="t-c">
                            Month
                        </td>

                        <td class="t-r">
                            Amount #
                        </td>
                    </tr>
                    <tr class="item">
                        <td>
                            Mobile Advertising Services
                        </td>
                        <td class="t-c">
                            {{ \Carbon\Carbon::parse($invoice['month'])->format('M-Y') }}
                        </td>
                        <td class="t-r">
                            ${{ number_format($invoice['amount']*1,3) }}
                        </td>
                    </tr>
                    <tr class="total">
                        <td></td>
                        <td></td>
                        <td class="f-16">
                            Total: ${{ number_format($invoice['amount']*1,3) }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="t-l">
                            <p style="margin-bottom: 16px"><b>Payment:</b></p>
                            Account Name: ADBINK LIMITED<br>
                            Account Number: NRA002504074055<br>
                            SWIFT: HSBCCNSHSZN<br>
                            Address: China Resources Building No.5001,shennan East Road Luohu District,Shenzhen<br>
                            Bank Name: HSBC Bank (China) Company Limited<br>
                            Paypal: oliver.l@adbinks.com<br>
                            Payoneer: finance@adbinks.com
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2" class="b-t">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="t-l" style="padding-bottom: 5px">
                            <p style="margin-bottom: 16px">Signature:</p>
                            <img src="data:image/jpeg;base64,{{ base64_encode(@file_get_contents(url('/assets/img/pdf/adbink_signature.jpg'))) }}" style="width: 120px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
</div>
</body>
</html>
