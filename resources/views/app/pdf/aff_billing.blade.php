<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        .invoice-box {
            max-width: 100%;
            margin: auto;
            /*padding: 30px;*/
            /*border: 1px solid #eee;*/
            /*box-shadow: 0 0 10px rgba(0, 0, 0, .15);*/
            font-size: 14px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.heading td {
            border-bottom: 1px solid #ddd;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            font-weight: bold;
        }
        .invoice-box table tr.heading td:last-child{
            border-right: 1px solid #ddd;
        }

        /*.invoice-box table tr.details td {*/
        /*    padding-bottom: 20px;*/
        /*}*/

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
            border-left: 1px solid #ddd;
        }
        .invoice-box table tr.item td:last-child{
            border-right: 1px solid #ddd;
        }

        /*.invoice-box table tr.item.last td {*/
        /*    border-bottom: none;*/
        /*}*/

        .invoice-box table tr.total td:last-child {
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
        .f-14{
            font-size: 14px
        }
        .f-16{
            font-size: 16px
        }
        .t-l{
            text-align: left!important;
        }
        .t-r{
            text-align: right!important;
        }
        .t-c{
            text-align: center!important;
        }
        .b-b{
            border-top: 1px solid #eee;
        }
        .b-t{
            border-top: 1px solid #eee;
        }
        p{
            margin: 0;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td style="font-size: 24px" class="t-l">
               [{{ \Carbon\Carbon::parse($invoice['month'])->firstOfMonth()->format('d/m/Y') }} - {{ \Carbon\Carbon::parse($invoice['month'])->lastOfMonth()->format('d/m/Y') }}] Billing
            </td>
        </tr>
        <tr class="information">
            <td style="padding-top: 24px" class="t-l">
                <b class="f-16">FROM:</b> {{ $entity_name }}
            </td>
        </tr>
        <tr class="information">
            <td class="t-l">
                <b class="f-16">TO:</b> {{ $invoice['affiliate']['company_name']?$invoice['affiliate']['company_name']:$invoice['affiliate']["name"] }}
            </td>
        </tr>
        <tr class="information">
            <td class="t-l">
                <b class="f-16">Amount:</b> USD{{ $invoice["confirm"] }}
            </td>
        </tr>
        <tr class="information">
            <td class="t-l">
                If any problem, contact us ASAP.
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 16px;font-size: 12px;">
        <tr class="heading">
            <td>Offer Id</td>
            <td>Offer Name</td>
            <td>In Type</td>
            <td>Total Conversion</td>
            <td>Total Revenue</td>
            <td>Reduce</td>
            <td>Pending</td>
            <td>Final Revenue</td>
            <td>Validate</td>
        </tr>
        @foreach($billings as $billing)
            <tr class="item">
                <td>{{ $billing["campaign"]['id'] }}</td>
                <td>{{ $billing['campaign']['name'] }}</td>
                @if(!!$billing['campaign']['api'])
                    <td>API</td>
                @else
                    <td>---</td>
                @endif
                <td>{{ $billing['callback'] }}</td>
                <td>{{ $billing['payment'] }}</td>
                <td>{{ $billing['pub_reduce'] }}</td>
                @if(!!$billing['status'])
                    <td>0</td>
                @else
                    <td>{{ $billing['payment'] }}</td>
                @endif
                @if(!!$billing['status'])
                    <td>{{ $billing['payment'] - $billing['pub_reduce'] }}</td>
                @else
                    <td>0</td>
                @endif
                @if(!!$billing['status'])
                    <td>Confirmed</td>
                @else
                    <td>Pending</td>
                @endif
            </tr>
        @endforeach
    </table>
</div>
</body>
</html>

