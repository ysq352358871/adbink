<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Appone System</title>
</head>
<body>
<div id="app">
    <embed style="min-height: 100vh;" src="{{ asset('document/AppOne_api.pdf') }}" type="application/pdf" width="100%">
</div>
</body>
</html>

