<!-- content -->
<div id="content" class="app-content" role="main" style="padding-bottom: 71px">

    @yield('content')

</div>
<!-- /content -->