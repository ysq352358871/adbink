{{--<li>--}}
    {{--<a href="{{ url('new') }}">--}}
        {{--<i class="fa fa-fw fa-plus"></i>--}}
        {{--<span>New</span>--}}
    {{--</a>--}}
{{--</li>--}}
<li class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
        <i class="fa fa-fw fa-plus"></i>
        <span>New</span> <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ route('offers.create') }}">Offer</a></li>
        <li>
            <a href="{{ route('advertisers.create') }}">
{{--                <span class="badge bg-info pull-right">5</span>--}}
                <span >Advertiser</span>
            </a>
        </li>
        <li><a href="{{ route('affiliates.create') }}">Affiliate</a></li>
        @if(\Auth::user()->role === 'admin')
        <li><a href="{{ route('users.create') }}">User</a></li>
        @endif
{{--        <li class="divider"></li>--}}
{{--        <li>--}}
{{--            <a href>--}}
{{--                <span class="badge bg-danger pull-right">4</span>--}}
{{--                <span translate="header.navbar.new.EMAIL">Email</span>--}}
{{--            </a>--}}
{{--        </li>--}}
    </ul>
{{--</li>--}}