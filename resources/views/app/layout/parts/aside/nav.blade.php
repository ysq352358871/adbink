<!-- nav -->
<nav ui-nav class="navi clearfix">
    <ul class="nav">
        <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
            <span>Navigation</span>
        </li>
        <li>
            <a href="{{ route('dashboard') }}" class="auto">
                <i class="fa fa-fw fa-dashboard icon {{ request()->url() == route('dashboard') ? 'text-primary-dker' : '' }}"></i>
                <span class="font-bold">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                <i class="glyphicon glyphicon-briefcase icon @if(request()->is('offers*')) text-primary-dker @endif"></i>
                <span>Offers</span>
            </a>
            <ul class="nav nav-sub dk">
                <li class="nav-sub-header">
                    <a href>
                        <span>Offers</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('offers') }}">
                        <span>Offers</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('offers.api') }}">
                        <span>API offers</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('offers.top') }}">
                        <span>Top offers</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('offers.applied') }}">
                        <span>Offer Applications</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                <i class="fa fa-fw fa-sitemap icon @if(request()->is('affiliates*')) text-primary-dker @endif"></i>
                <span>Affiliates</span>
            </a>
            <ul class="nav nav-sub dk">
                <li class="nav-sub-header">
                    <a href>
                        <span>Affiliates</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('affiliates') }}">
                        <span>Affiliates</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                <i class="fa fa-fw fa-users @if(request()->is('advertisers*')) text-primary-dker @endif"></i>
                <span>Advertisers</span>
            </a>
            <ul class="nav nav-sub dk">
                <li class="nav-sub-header">
                    <a href>
                        <span>Advertisers</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('advertisers') }}">
                        <span>Advertisers</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('automation') }}" class="auto">
                <i class="fa fa-fw fa-support icon {{ request()->url() == route('automation') ? 'text-primary-dker' : '' }}"></i>
                <span>Automation</span>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                <i class="glyphicon glyphicon-stats @if(request()->is('report*')) text-primary-dker @endif"></i>
                <span>Report</span>
            </a>
            <ul class="nav nav-sub dk">
                <li class="nav-sub-header">
                    <a href>
                        <span>Report</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('report.general') }}">
                        <span>General report</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('report.conversion') }}">
                        <span>Conversion report</span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:;" class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                <i class="fa fa-fw fa-book @if(request()->is('billing*')) text-primary-dker @endif"></i>
                <span>Billing</span>
            </a>
            <ul class="nav nav-sub dk">
                <li class="nav-sub-header">
                    <a href>
                        <span>Billing</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('billing.advertiser') }}">
                        <span>Advertiser's billing</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('billing.affiliate') }}">
                        <span>Affiliate's billing</span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:;" class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                <i class="fa fa-fw fa-file-text @if(request()->is('invoice*')) text-primary-dker @endif"></i>
                <span>Invoice</span>
            </a>
            <ul class="nav nav-sub dk">
                <li class="nav-sub-header">
                    <a href>
                        <span>Invoice</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('invoice.advertiser') }}">
                        <span>Advertiser's invoice</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('invoice.affiliate') }}">
                        <span>Affiliate's invoice</span>
                    </a>
                </li>
                @if(\Auth::user()->user_id === 58)
                <li>
                    <a href="{{ route('outlay.index') }}" class="auto">
                        <span>Outlay</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('finance.index') }}" class="auto">
                        <span>Finance View</span>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        <li>
            <a href="{{ route('import.billing') }}" class="auto">
                <i class="fa fa-fw fa-file icon {{ request()->url() == route('import.billing') ? 'text-primary-dker' : '' }}"></i>
                <span class="font-bold">Import billing</span>
            </a>
        </li>
{{--        <li>--}}
{{--            <a href class="auto">--}}
{{--                  <span class="pull-right text-muted">--}}
{{--                    <i class="fa fa-fw fa-angle-right text"></i>--}}
{{--                    <i class="fa fa-fw fa-angle-down text-active"></i>--}}
{{--                  </span>--}}
{{--                <i class="glyphicon glyphicon-file icon"></i>--}}
{{--                <span>Pages</span>--}}
{{--            </a>--}}
{{--            <ul class="nav nav-sub dk">--}}
{{--                <li class="nav-sub-header">--}}
{{--                    <a href>--}}
{{--                        <span>Pages</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_profile.html">--}}
{{--                        <span>Profile</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_post.html">--}}
{{--                        <span>Post</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_search.html">--}}
{{--                        <span>Search</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_invoice.html">--}}
{{--                        <span>Invoice</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_price.html">--}}
{{--                        <span>Price</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_lockme.html">--}}
{{--                        <span>Lock screen</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_signin.html">--}}
{{--                        <span>Signin</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_signup.html">--}}
{{--                        <span>Signup</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_forgotpwd.html">--}}
{{--                        <span>Forgot password</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="page_404.html">--}}
{{--                        <span>404</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}
    @if(\Auth::user()->role === 'admin')
        <li class="line dk hidden-folded"></li>

{{--        <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">--}}
{{--            <span>Your Stuff</span>--}}
{{--        </li>--}}
        <li>
            <a href="{{ route('users') }}">
                <i class="icon-user icon text-success-lter"></i>
                <span>Users</span>
            </a>
        </li>
    @endif
    </ul>
</nav>
<!-- nav -->