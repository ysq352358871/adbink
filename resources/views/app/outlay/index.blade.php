@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3">Outlay</h1>
    </div>
    <div class="wrapper-md">
        <outlay-list></outlay-list>
    </div>
@endsection
