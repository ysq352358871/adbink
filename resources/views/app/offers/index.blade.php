@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3">Offers</h1>
        <a href="{{ route('offers.create') }}"
           class="btn btn-sm btn-primary"
           target="_blank">Create offer</a>
    </div>
    <div class="wrapper-md">
        <offers-list></offers-list>
    </div>
@endsection
