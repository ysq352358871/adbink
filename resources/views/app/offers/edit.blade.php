@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3 pull-left">
            {{ $offer->name }}
        </h1>
        <ul class="breadcrumb pull-right" style="margin-bottom: 0">
            <li><a href="/offers">Offers</a></li>
            <li><a href="/offers/{{ $offer->id }}">{{ $offer->name }}</a></li>
            <li style="color: #3da2df">{{ $offer->name }}</li>
        </ul>
    </div>
    <div class="wrapper-md">
        <offers-edit :id="{{ $offer_id }}"></offers-edit>
    </div>
@endsection


