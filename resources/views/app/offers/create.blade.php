@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3">Create offer</h1>
    </div>
    <div class="wrapper-md">
        <offers-create @if($offer_id) :id="{{ $offer_id }}" @endif init_adv_oid="{{ $advOid }}"></offers-create>
    </div>
@endsection

