@extends('layouts.affiliate.app')

@push('style')
    <link rel="stylesheet" href="{{ asset('assets/auth/style.css') }}">
    <link href="{{ asset('assets/auth/css2.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="main">
    <div class="container a-container" id="a-container">

        <form class="form" id="a-form" method="POST" action="{{ url('affiliate/login') }}">
            {{ csrf_field() }}
            <h2 class="form_title title">Sign in</h2>
            <span class="form__span">Use your email account</span>
            @if (!empty($errors))
                <div class="text-danger wrapper text-center">
                    {{ $errors->first() }}
                </div>
            @endif
            <input class="form__input" name="email" type="text" placeholder="Email">
            <input class="form__input" name="password" type="password" placeholder="Password">
            <a class="form__link" href="{{ route('Affiliate.password.request') }}">Forget password?</a>
            <button class="button" type="submit" style="cursor: pointer">Sign in</button>
        </form>
    </div>
    <div class="container b-container" id="b-container">
        <form class="form" id="b-form" method="POST" action="{{ route('affiliate.register') }}">
            {{ csrf_field() }}
            <h2 class="form_title title">Create an account</h2>
            <span class="form__span">Register using email</span>
            @if (!empty($errors))
                <div class="text-danger wrapper text-center" style="color: #f56c6c">
                    {{ $errors->first() }}
                </div>
            @endif
            @if (session('error'))
                <div class="text-danger wrapper text-center" style="color: #f56c6c">
                    {{ session('error') }}
                </div>
            @endif
            <input class="form__input" type="text" name="name" placeholder="Name" required>
            <input class="form__input" type="email" name="email" value="{{ old('email') }}" placeholder="Email" required>
            <input class="form__input" type="password" name="password" placeholder="Password" required>
            <input class="form__input" type="password" name="password_confirmation"  placeholder="Confirm Password" required>
            <button class="form__button button" type="submit" style="cursor: pointer">Sign up now</button>
        </form>
    </div>
    <div class="switch" id="switch-cnt">
        <div class="switch__circle">

        </div>
        <div class="switch__circle switch__circle--t">

        </div>
        <div class="switch__container" id="switch-c1">
            <h2 class="switch__title title">Adbink</h2>
            <p class="switch__description description">Enter your personal information and start communicating with us</p>
            <button class="switch__button button switch-btn">Sign up now</button>
        </div>
        <div class="switch__container is-hidden" id="switch-c2">
            <h2 class="switch__title title">Welcome back!</h2>
            <p class="switch__description description">To keep in touch with us, please log in your personal information</p>
            <button class="switch__button button switch-btn">Sign in</button>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/auth/main.js') }}"></script>
@endpush
