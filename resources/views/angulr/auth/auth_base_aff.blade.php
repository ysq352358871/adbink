@extends('angulr.layout.master')

@section('div.app.class', 'app-header-fixed')

@section('app')
    <div class="container w-xxl w-auto-xs">
        <a href="{{ route('affiliate.home') }}" class="navbar-brand block m-t">
            Adbink
        </a>
        <div class="m-b-lg">
            @yield('content')
        </div>
        <div class="text-center">
            <p>
                <small class="text-muted">
                    @include('angulr.layout.parts.footer.copyright')
                </small>
            </p>
        </div>
    </div>
@endsection
