@component('mail::message')

@slot('header')
@component('mail::header', ['url' => config('app.url')])
    Support
@endcomponent
@endslot

You are receiving this email because we received a password reset request for your account.

@component('mail::button', ['url' => $url])
    Reset Password
@endcomponent

If you did not request a password reset, no further action is required.<br><br>

Regards,<br>
{{--{{ config('app.name') }}--}}


@component('mail::subcopy')
    If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: {{ $url }}
@endcomponent

@slot('footer')
@component('mail::footer')
    © {{ date('Y') }} {{ ucfirst($adv->platform) }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
