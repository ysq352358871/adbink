@component('mail::message')

@slot('header')
@component('mail::header', ['url' => $platform_url])
    {{ ucfirst($invoice['affiliate']['platform']) }}
@endcomponent
@endslot

Dear {{ $invoice['affiliate']['company_name']??'Publisher/Developer' }},

Attachment is confirmed billing numbers of {{ \Carbon\Carbon::parse($invoice['month'])->format('M Y') }},

Approved revenue is USD {{ $invoice['confirm'] }}.

Pending amount is USD {{ $invoice['pending'] ?? 0 }}.

Rejected amount is USD {{ $invoice['reduce'] }}.

Cover amount is USD {{ $invoice['cover_amount'] }}.

Please check attachment for details.<br>

Feel free to contact us ,if you have any questions regarding on this.
If no problem, please send invoice to finance@adbinks.com.<br>

Invoice Info:<br>
@if($invoice['affiliate']['platform'] === 'adbink')
Company Name: ADBINK LIMITED.<br>
Address: FLAT/RM A 12/F KIU FU COMMERCIAL BLDG 300 LOCKHAART ROAD WAN CHAI HK.
@else
Company Name: SELECTAD GROUP LIMITED.<br>
Address: FLAT B5,1/F MANNING IND BLDG 116-118 HOW MING ST KWUN TONG KL HK.
@endif


Best Regards,<br>
@if($invoice['affiliate']['platform'] === 'adbink')
Adbink Limited.
@else
SELECTAD GROUP LIMITED.
@endif
@endcomponent
