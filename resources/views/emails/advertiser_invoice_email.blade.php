@component('mail::message')

@slot('header')
@component('mail::header', ['url' => $platform_url])
    {{ ucfirst($adv->platform) }}
@endcomponent
@endslot

Dear {{ $adv->company_name }},

Thank you for your business. Your invoice can be viewed, printed and downloaded as PDF from the link below.

@component('mail::button', ['url' => $invoice["link"]])
    Invoice: {{ $invoice_no }}
@endcomponent

Invoice Date: {{ \Carbon\Carbon::parse($invoice['invoice_date'])->format('d M Y') }}<br>
Due Date: {{ \Carbon\Carbon::parse($invoice['invoice_date'])->addMonth(1)->format('d M Y') }}<br>
INVOICE AMOUNT: ${{ number_format($invoice['amount']*1,3) }}<br><br>

Regards,<br>
Finance Team.<br>
Email: finance@adbinks.com
{{--{{ config('app.name') }}--}}


@component('mail::subcopy')
    If you’re having trouble clicking the "Invoice: {{ $invoice_no }}" button, copy and paste the URL below into your web browser: {{ $invoice["link"] }}
@endcomponent

@slot('footer')
@component('mail::footer')
    © {{ date('Y') }} {{ ucfirst($adv->platform) }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
