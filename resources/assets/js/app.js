
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import moment from 'moment';

import ElementUI from "element-ui";
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'

Vue.use(ElementUI, { locale });

import VueClipboard from 'vue-clipboard2';
Vue.use(VueClipboard);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('dashboard',require("./components/app/Dashboard").default);



Vue.component('offers-list',require("./components/app/offers/OfferList").default);
Vue.component('top-offers',require("./components/app/offers/TopOffers").default);
Vue.component('offers-edit',require("./components/app/offers/OfferEdit").default);
Vue.component('offers-create',require("./components/app/offers/OfferCreate").default);
Vue.component('applied-list',require("./components/app/offers/AppliedList").default);
Vue.component('offer-show',require("./components/app/offers/OfferShow").default);

Vue.component('affiliate-list',require("./components/app/affiliates/AffiliateList").default);
Vue.component('affiliate-edit',require("./components/app/affiliates/AffiliateEdit").default);
Vue.component('affiliate-show',require("./components/app/affiliates/AffiliateShow").default);


Vue.component('advertisers-edit',require("./components/app/advertisers/AdvertisersEdit").default);
Vue.component('advertisers-list',require("./components/app/advertisers/AdvertisersList").default);


Vue.component('general-report',require("./components/app/report/GeneralReport").default);
Vue.component('conversion-report',require("./components/app/report/ConversionReport").default);


Vue.component('advertiser-list',require("./components/app/billing/AdvertiserList").default);
Vue.component('advertiser-show',require("./components/app/billing/AdvertiserShow").default);
Vue.component('billing-affiliate-list',require("./components/app/billing/AffiliateList").default);
Vue.component('billing-affiliate-show',require("./components/app/billing/AffiliateShow").default);


Vue.component('invoice-advertiser',require("./components/app/invoice/AdvertiserList").default);
Vue.component('invoice-affiliate',require("./components/app/invoice/AffiliateList").default);


Vue.component('auto-index',require("./components/app/automation/AutoIndex").default);

Vue.component('import-billing',require("./components/app/ImportBilling").default);

Vue.component('crm',require("./components/app/components/CRM").default);
Vue.component('crm-drawer',require("./components/app/components/CrmDrawer").default);
Vue.component('outlay-list',require("./components/app/outlay/OutlayList").default);
Vue.component('finance-list',require("./components/app/finance/index").default);





Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('sl-user-list', require('./components/app/users/UserList.vue').default);
Vue.component('user-create', require('./components/app/users/Create').default);
Vue.component('user-profile', require('./components/app/users/UserProfile').default);

// Vuex
// import Vuex from 'vuex'
// const vuexStore = new Vuex.Store({
//     state: {
//         platform: 'app',
//         count: 0
//     },
//     mutations: {
//         increment (state) {
//             state.count++
//         }
//     }
// });
// window.vuexStore = vuexStore;

import ECharts from 'vue-echarts/components/ECharts'

// import ECharts modules manually to reduce bundle size
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'

// register component to use
Vue.component('chart', ECharts);

const app = new Vue({
    el: '#app',
    data: {
        msg: "hello",
    },
    computed: {},
    watch: {},
    events: {},
    created() {
        console.log('Bootstrap.');
        this.initLocale();
    },
    mounted() {
        console.log('Ready.');
    },
    methods: {
        initLocale() {
            console.log('Init Locale.');

            var that = this;
            var lang = this.locale;

            Vue.config.lang = lang;
            Vue.locale(lang, window.Someline.locales);

        },
        incrementTotal(){
            console.log(123);
        }
    }
});
